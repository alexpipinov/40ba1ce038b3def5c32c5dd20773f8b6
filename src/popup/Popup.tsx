import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";

import Preloading from "../shared/components/Preloading";
import PopupRegistrationInfo from "./components/PopupRegistrationInfo";
import PopupErrorState from "./components/PopupErrorState";
import PopupYClientsPlaceInfo from "./components/PopupYClientsPlaceInfo";
import PopupActiveFuncs from "./components/PopupActiveFuncs";
import InfoRow from "../shared/components/InfoRow";

import * as PL_CNT from "../shared/entities/place/place.constants";
import * as PL_TPS from "../shared/entities/place/place.types";
import * as YC_TPS from "../shared/entities/yclients/yclients.types";

import { placeGetStatus } from "../shared/entities/place/place";
import { yClientsGetPlaceInfo } from "../shared/entities/yclients/yclients";

import "@fontsource/roboto";
import "../shared/styles/index";
import "./Popup.styles.scss";

const App: React.FC<{}> = () => {
  const [abkaPlaceStatus, setAbkaPlaceStatus] = useState<PL_TPS.PlaceStatus | null>(null);
  const [yClientsPlaceInfo, setYClientsPlaceInfo] = useState<YC_TPS.YClientsPlaceInfo | null>(null);

  useEffect(() => {
    placeGetStatus().then((status) => setAbkaPlaceStatus(status));
    yClientsGetPlaceInfo().then((info) => setYClientsPlaceInfo(info));
  }, []);

  function canRenderPlaceInfo() {
    return yClientsPlaceInfo && abkaPlaceStatus !== PL_CNT.PLACE_STATUS_ERROR;
  }

  function renderStatusByType() {
    switch (abkaPlaceStatus) {
      case PL_CNT.PLACE_STATUS_ACTIVE:
        return renderStatus("авторизован", "success");
      case PL_CNT.PLACE_STATUS_NOT_REGISTERED:
        return renderStatus("не зарегистрирован", "error");
      default:
        return null;
    }
  }

  function renderStatus(
    status: string,
    highlightType?: "success" | "error" | undefined
  ) {
    return (
      <div className="b-abka-popup__status-wrap">
        <InfoRow
          label="Статус филиала:"
          value={status}
          valueType="text"
          highlightError={highlightType === "error"}
          highlightForSuccess={highlightType === "success"}
        />
      </div>
    );
  }

  function renderContent() {
    switch (abkaPlaceStatus) {
      case PL_CNT.PLACE_STATUS_ACTIVE:
        return <PopupActiveFuncs />;
      case PL_CNT.PLACE_STATUS_NOT_REGISTERED:
        if (yClientsPlaceInfo) {
          return (
            <PopupRegistrationInfo yClientsPlaceInfo={yClientsPlaceInfo} />
          );
        }
        return null;
      case PL_CNT.PLACE_STATUS_ERROR:
        return <PopupErrorState />;
      default:
        return null;
    }
  }

  if (!abkaPlaceStatus || !yClientsPlaceInfo) {
    return (
      <div className="b-abka-default-styles">
        <div className="b-abka-popup">
          <Preloading size={35} />
        </div>
      </div>
    );
  }

  return (
    <div className="b-abka-default-styles">
      <div className="b-abka-popup">
        {renderStatusByType()}

        {!canRenderPlaceInfo() ? null : (
          <PopupYClientsPlaceInfo yClientsPlace={yClientsPlaceInfo} />
        )}

        {renderContent()}

        <div className="b-abka-popup__version">
          Версия: {ABKA_PLUGIN_VERSION}
        </div>
      </div>
    </div>
  );
};

const root = document.createElement("div");
document.body.appendChild(root);
ReactDOM.render(<App />, root);
