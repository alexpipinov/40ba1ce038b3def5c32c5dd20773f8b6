import React from 'react';

export type PopupNoteType = 'error' | 'info';

export type PopupNoteProps = PopupNoteOwnProps;

export interface PopupNoteOwnProps {
  type?: PopupNoteType;
  children: React.ReactNode;
}

export interface PopupNoteDefaultProps {
  type: PopupNoteType
}

