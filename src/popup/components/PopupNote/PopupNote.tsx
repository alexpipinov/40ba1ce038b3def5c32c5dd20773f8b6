import React from 'react';
import classNames from 'classnames';

import * as TPS from './PopupNote.types';

import './PopupNote.styles.scss';

const PopupNote: React.FC<TPS.PopupNoteProps> = (props) => {
  const { type, children } = props;

  const containerStyles = classNames('b-popup-note', {
    'b-popup-note_type_error': type === 'error',
    'b-popup-note_type_info': type === 'info'
  });

  return (
    <div className={containerStyles}>
      {children}
    </div>
  )
}

PopupNote.defaultProps = {
  type: 'info'
};

export default PopupNote;
