import React, { useCallback } from 'react';

import Button from '../../../shared/components/Button';

const PopupExport = () => {
  const startExport = useCallback(async () => {
    chrome.tabs.query({currentWindow: true, active: true}, function (tabs) {
      const activeTab = tabs[0];

      if (activeTab && typeof activeTab.id === 'number') {
        chrome.tabs.sendMessage(activeTab.id, {
          data: {
            type: 'P_EXPORT'
          }
        });
      }
    });
  }, []);

  return (
    <Button
      label="Экспорт услуг"
      bgColor="green"
      size="big"
      isFetching={false}
      handlerSubmit={startExport}
    />
  );
}

export default PopupExport;
