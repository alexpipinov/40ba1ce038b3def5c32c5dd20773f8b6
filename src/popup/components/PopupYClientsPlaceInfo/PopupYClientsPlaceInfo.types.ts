import * as YC_TPS from '../../../shared/entities/yclients/yclients.types';

export interface PopupYclientsPlaceInfoProps {
  yClientsPlace: YC_TPS.YClientsPlaceInfo
}
