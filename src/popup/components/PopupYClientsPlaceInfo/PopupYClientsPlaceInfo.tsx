import React from 'react';

import * as TPS from './PopupYClientsPlaceInfo.types';

import InfoRow from '../../../shared/components/InfoRow';

import './PopupYClientsPlaceInfo.styles.scss';

const PopupYClientsPlaceInfo: React.FC<TPS.PopupYclientsPlaceInfoProps> = (props) => {
  const { yClientsPlace } = props;

  function isValidValue(value: string | number) {
    return value !== '-';
  }

  return (
    <div className="b-popup-yclients-place-info">
      <InfoRow
        label="ID филиала:"
        value={yClientsPlace.id}
        valueType="text"
      />

      <InfoRow
        label="Город:"
        value={yClientsPlace.city}
        valueType="text"
        highlightError={!isValidValue(yClientsPlace.city)}
      />

      <InfoRow
        label="Адрес:"
        value={yClientsPlace.address}
        valueType="text"
        highlightError={!isValidValue(yClientsPlace.address)}
      />

      <InfoRow
        label="Телефон:"
        value={yClientsPlace.phone}
        valueType="text"
        highlightError={!isValidValue(yClientsPlace.phone)}
      />

      <InfoRow
        label="Широта:"
        value={yClientsPlace.long}
        valueType="text"
        highlightError={!isValidValue(yClientsPlace.lat)}
      />

      <InfoRow
        label="Долгота:"
        value={yClientsPlace.lat}
        valueType="text"
        highlightError={!isValidValue(yClientsPlace.lat)}
      />
    </div>
  );
}

export default PopupYClientsPlaceInfo;
