import React, { useState, useCallback } from 'react';

import Button from '../../../shared/components/Button';

import { serviceClearCash, serviceReloadPage, serviceClosePopup } from '../../../shared/lib/services/services';

const PopupClearCash = () => {
  const [isFetching, setFetching] = useState(false);

  const clearCash = useCallback(async () => {
    setFetching(true);

    await serviceClearCash();
    serviceClosePopup();
    await serviceReloadPage();

    setFetching(false);
  }, []);

  return (
    <Button
      label="Сбросить кеш"
      bgColor="gray"
      size="big"
      isFetching={isFetching}
      handlerSubmit={clearCash}
    />
  );
}

export default PopupClearCash;
