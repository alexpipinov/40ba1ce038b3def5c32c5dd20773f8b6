import React from "react";

import PopupNote from "../../components/PopupNote";
import PopupClearCash from "../PopupClearCash";

import "./PopupErrorState.styles.scss";

const PopupErrorState: React.FC<{}> = (props) => {
  return (
    <div className="b-popup-error-state">
      <PopupNote>
        Во время инициализации плагина произошла ошибка. Попробуйте
        перезагрузить страницу или сбросить кеш.
      </PopupNote>

      <div className="b-popup-error-state__btn-wrap">
        <PopupClearCash />
      </div>
    </div>
  );
};

export default PopupErrorState;
