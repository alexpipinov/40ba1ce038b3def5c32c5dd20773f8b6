import React, { useCallback }  from 'react';

import * as TPS from './PopupRegistrationInfo.types';
import * as YC_TPS from '../../../shared/entities/yclients/yclients.types';

import Button from '../../../shared/components/Button';
import PopupNote from '../PopupNote/PopupNote';

import './PopupRegistrationInfo.styles.scss';

const PopupRegistrationInfo: React.FC<TPS.PopupRegistrationInfoProps> = (props) => {
  const { yClientsPlaceInfo } = props;

  const generateRegData = () => {
    const { id, city, address, phone, long, lat } = yClientsPlaceInfo as YC_TPS.YClientsPlaceInfo;

    return `ID филиала: ${id}. Город: ${city}. Адрес: ${address}. Телефон: ${phone}. Широта: ${long}. Долгота: ${lat}`;
  }

  const submit = useCallback(() => {
    window.open(`https://wa.me/${ABKA_SUPPORT_PHONE}?text=[Регистрация филиала]: ${generateRegData()}`);
  }, [yClientsPlaceInfo]);

  return (
    <div className="b-popup-registration-info">
      <PopupNote>
        <div className="b-popup-registration-info__info-wrap">
          <div className="b-popup-registration-info__note">
            Для регистрации филиала необходимо обратиться в поддержку «АбсолютКарта». В сообщении необходимо указать
            все вышеперечисленные данные.
          </div>

          <div className="b-popup-registration-info__note">
            Сообщение можно отправить на почту <a className="b-abka-link _td" href="mailto:support@abka.su">support@abka.su</a>,
            или в  WhatsApp.
          </div>
        </div>
      </PopupNote>

      <div className="b-popup-registration-info__btn-wrapper">
        <Button
          label="Отправить в WhatsApp"
          size="big"
          isFetching={false}
          handlerSubmit={submit}
        />
      </div>
    </div>
  );
}

export default PopupRegistrationInfo;
