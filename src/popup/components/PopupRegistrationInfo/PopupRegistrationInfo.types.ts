import * as YC_TPS from '../../../shared/entities/yclients/yclients.types';

export interface PopupRegistrationInfoProps {
  yClientsPlaceInfo: YC_TPS.YClientsPlaceInfo
}
