import React from 'react';

import PopupClearCash from '../../components/PopupClearCash';
import PopupExport from '../../components/PopupExport';

import './PopupActiveFuncs.styles.scss';

const PopupActiveFuncs: React.FC<{}> = () => {
  return (
    <div className="b-popup-active-funcs">
      <div className="b-popup-active-funcs__btn-wrap">
        <PopupExport />
      </div>
      <div className="b-popup-active-funcs__btn-wrap">
        <PopupClearCash />
      </div>
    </div>
  );
}




export default PopupActiveFuncs;
