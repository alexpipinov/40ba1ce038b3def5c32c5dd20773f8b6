import * as TPS from './client.types';

import { callAbKa } from '../../lib/api/abka/abka.api.call';

import { dispatch } from '../../store/configureStore';
import { setClient } from '../processes/processes.state';

const unknownErrorState = {
  status: 500,
  success: false,
  data: null,
  messages: [{
    field: 'phone',
    message: 'Неизвестная ошибка. Обновите страницу и попробуйте снова'
  }]
};

const normalizeClientData: TPS.ClientNormalizeData = async (clientData) => {
  const { interactionType, phone, appInstalled } = clientData.user;

  return {
    phone,
    interactionType,
    appInstalled
  }
}

export const clientIdentify: TPS.ClientIdentify = async (uid, phone) => {
  try {
    const data = { uid, data: { userExtId: phone } };
    const profileResponse = await callAbKa('[endpoint]', data);

    if (profileResponse.success) {
      const clientData = await normalizeClientData(profileResponse.data);
      dispatch(setClient(clientData));
    }

    return profileResponse;
  } catch (error) {
    return unknownErrorState;
  }
}

export const clientSendAppInstallLink: TPS.ClientSendAppInstallLink = async (uid, phone) => {
  try {
    const data = { uid, data: { userExtId: phone } };

    return await callAbKa('endpoint', data);
  } catch (error) {
    return unknownErrorState;
  }
}
