export interface ClientData {
  dealings: ClientDealings;
  user: Client;
}

export interface ClientDealings {
  id: number;
  bill: number;
  deferredBill: number;
  unconfirmedBill: number;
  unconfirmedDeferredBill: number;
}

export interface Client {
  id: number;
  interactionType: ClientInteractionType;
  appInstalled: 1 | 0;
  phone: number;
}

export interface ClientNormalizedData {
  interactionType: ClientInteractionType;
  appInstalled: 1 | 0;
  phone: number;
}

export type ClientInteractionType = 'full' | 'reduced';

export type ClientIdentify = (uid: number, phone: string) => Promise<ApiRequestNormalizedResponse>;
export type ClientSendAppInstallLink = (uid: number, phone: number) => Promise<ApiRequestNormalizedResponse>;
export type ClientNormalizeData = (data: ClientData) => Promise<ClientNormalizedData>;
