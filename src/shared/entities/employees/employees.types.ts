import * as CNT from './employees.constants';

export interface EmployeesState {
  [key: string]: EmployeeCheckInResult;
}

export interface EmployeeYClients {
  id: number;
  phone: string | undefined;
  name: string;
}

export type EmployeeAbkaStatus = (
  typeof CNT.EMPLOYEES_STATUS_REGISTERED
  | typeof CNT.EMPLOYEES_STATUS_NOT_REGISTERED
  | typeof CNT.EMPLOYEES_STATUS_BLOCKED
  | typeof CNT.EMPLOYEES_STATUS_ERROR
);

export interface EmployeeCheckInResult {
  status: EmployeeAbkaStatus;
  data?: EmployeeYClients;
  message?: string;
}

export interface EmployeeRegisterData {
  method: 'sms';
  name: string;
  phone: string;
}

export interface EmployeeSyncData {
  name: string;
  phone: string;
  code: string;
}

export type EmployeeCheckIn = (id?: number) => Promise<EmployeeCheckInResult>;

export type EmployeeCheckInAbka = (uid?: number) => Promise<EmployeeCheckInResult>;
export type EmployeeSave = (uid: number, data: EmployeeCheckInResult) => Promise<void>;
export type EmployeeFilterYClientsUsers = (users: any[], uid: number) => EmployeeYClients;
export type EmployeeDefineReadableStatus = (responseStatus: number) => EmployeeAbkaStatus;
export type EmployeeGetYClientsStaffInfo = (uid: number) => Promise<EmployeeYClients | null>;
export type EmployeeRegister = (data: EmployeeRegisterData, masterId?: number | null) => Promise<ApiRequestNormalizedResponse>
export type EmployeeSync = (data: EmployeeSyncData, masterId?: number | null) => Promise<ApiRequestNormalizedResponse>
export type EmployeeAbkaRequest = (endpoint: string, data: any, masterId?: number | null) => Promise<ApiRequestNormalizedResponse>;
