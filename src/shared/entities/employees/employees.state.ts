import { createSlice } from '@reduxjs/toolkit';

import * as TPS from './employees.types';

const initialState: TPS.EmployeesState = {};

const employeesSlice = createSlice({
  name: 'employees',
  initialState,
  reducers: {
    employeeCreate: (state, action) => {
      const { uid, data } = action.payload;

      return {
        ...state,
        [uid]: data
      };
    },
    employeeUpdateStatus: (state, action) => {
      const { uid, status } = action.payload;

      return {
        ...state,
        [uid]: {
          ...state[uid],
          status
        }
      }
    },
    employeeUpdateData: (state, action) => {
      const { uid, data } = action.payload;

      return {
        ...state,
        [uid]: {
          ...state[uid],
          data: {
            ...state[uid]['data'],
            ...data
          }
        }
      }
    }
  }
});

export const { employeeCreate, employeeUpdateStatus, employeeUpdateData  } = employeesSlice.actions;

export default employeesSlice.reducer;
