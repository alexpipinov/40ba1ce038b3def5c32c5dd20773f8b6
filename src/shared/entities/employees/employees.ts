import * as CNT from './employees.constants';
import * as TPS from './employees.types';

import { yClientsGetState } from '../yclients/yclients';
import { employeeCreate } from './employees.state';

import * as API_CNT from '../../lib/api/abka/abka.api.constants';
import { callAbKa } from '../../lib/api/abka/abka.api.call';
import { callYClients  } from '../../lib/api/yclients/yclients.api.call';

import store, { dispatch } from '../../store/configureStore';

const employeeFilterYClientsUsers: TPS.EmployeeFilterYClientsUsers = ((
  users,
  targetId
) => {
  return users.filter(user => user.id === targetId)[0];
});

export const employeeSaveInStore: TPS.EmployeeSave = async (uid, data) => {
  dispatch(employeeCreate({ uid, data }));
}

export const employeeGetYClientsAdminInfo = async() => {
  const yClientsState = await yClientsGetState();
  const endpoint = `company_users/${yClientsState.salonId}`;

  const response = await callYClients(endpoint);

  if (response.success) {
    const user = await employeeFilterYClientsUsers(response.data, yClientsState.uid);

    return {
      id: user.id,
      phone: user.phone,
      name: user.name
    };
  }

  return null;
}

export const employeeGetYClientsStaffInfo: TPS.EmployeeGetYClientsStaffInfo = async (uid) => {
  const yClientsState = await yClientsGetState();
  const endpoint = `company/${yClientsState.salonId}/staff/${uid}`;

  const response = await callYClients(endpoint);

  if (response.success) {
    const staff = response.data;

    return {
      id: staff.id,
      phone: staff.phone,
      name: staff.name
    };
  }

  return null;
}

const employeeDefineReadableStatus: TPS.EmployeeDefineReadableStatus = (
  responseStatus
) => {
  switch (responseStatus) {
    case API_CNT.RESPONSE_STATUS_SUCCESS:
      return CNT.EMPLOYEES_STATUS_REGISTERED;
    case API_CNT.RESPONSE_STATUS_NOT_FOUND:
      return CNT.EMPLOYEES_STATUS_NOT_REGISTERED;
    case API_CNT.RESPONSE_STATUS_FORBIDDEN:
      return CNT.EMPLOYEES_STATUS_BLOCKED;
    default:
      return CNT.EMPLOYEES_STATUS_ERROR;
  }
}

const employeeCheckInAbka: TPS.EmployeeCheckInAbka = async ( uid) => {
  const endpoint = 'endpoint';
  const response = await callAbKa(endpoint, { uid });

  return {
    status: employeeDefineReadableStatus(response.status)
  };
};

/**
 * CheckIn employee
 *
 * 1. first of all, wa are checking storage
 * 2. if not success, checking in AbKa
 * 3. if not success, return not-registered status
 *
 * @param id
 */
export const employeeCheckIn: TPS.EmployeeCheckIn = async (id) => {
  const yClientsState = await yClientsGetState();
  const uid = id || yClientsState.uid || ms.uid;

  if (!uid) {
    return {
      status: CNT.EMPLOYEES_STATUS_ERROR,
      message: 'Произошла ошибка инициализации сотрудника. Перезагрузите страницу и попробуйте снова'
    } as TPS.EmployeeCheckInResult;
  }

    const { status } = await employeeCheckInAbka(uid);
    const result = { status } as TPS.EmployeeCheckInResult;

    await employeeSaveInStore(uid, result);

    return result;
}

export const employeeAbKaRequest: TPS.EmployeeAbkaRequest = async (endpoint, data, masterId = null) => {
  const yClientsState = await yClientsGetState();
  const uid = masterId || yClientsState.uid || ms.uid;

  return callAbKa(endpoint, { uid, data });
}

export const employeeRegister: TPS.EmployeeRegister = async (data, masterId = null) => {
  return employeeAbKaRequest('employee/sync/confirmationRequest', data, masterId);
}

export const employeeSync: TPS.EmployeeSync = async (data, masterId = null) => {
  return employeeAbKaRequest('employee/sync', data, masterId);
}
