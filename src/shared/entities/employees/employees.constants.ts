export const EMPLOYEES_STORAGE_KEY = 'abka-employees';

export const EMPLOYEES_STATUS_REGISTERED = 'registered';
export const EMPLOYEES_STATUS_NOT_REGISTERED = 'not-registered';
export const EMPLOYEES_STATUS_BLOCKED = 'blocked';
export const EMPLOYEES_STATUS_ERROR = 'error';

export const EMPLOYEE_TYPE_STAFF = 'staff';
export const EMPLOYEE_TYPE_ADMIN = 'administrator';
