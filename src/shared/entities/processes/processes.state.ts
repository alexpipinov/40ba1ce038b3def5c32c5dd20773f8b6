import { createSlice } from '@reduxjs/toolkit';

import * as CNT from './processes.constants';
import * as TPS from './processes.types';

const initialState: TPS.ProcessesState = {
  activeProcessId: null,
  list: {}
};

const processes = createSlice({
  name: 'processes',
  initialState,
  reducers: {
    initProcess: (state, action) => {
      const processId  = action.payload;

      state.activeProcessId = +processId;

      if (!state.list[processId]) {
        state.list[processId] = {
          id: +processId,
          status: CNT.PROCESSES_STATUS_INITIALIZING,
          calculationTime: Math.round(Date.now() / 1000),
          discountType: null,
          discountsList: null,
          client: null
        }
      }
    },
    setActiveProcessId: (state, action) => {
      state.activeProcessId = action.payload;
    },
    setStatus: (state, action) => {
      const activeProcessId = state.activeProcessId as number;

      state.list[activeProcessId].status = action.payload;
    },
    setClient: (state, action) => {
      const activeProcessId = state.activeProcessId as number;

      state.list[activeProcessId].client = action.payload;
    },
    setDiscountType: (state, action) => {
      const activeProcessId = state.activeProcessId as number;

      state.list[activeProcessId].discountType = action.payload;
    },
    setDiscountsList: (state, action) => {
      const activeProcessId = state.activeProcessId as number;

      state.list[activeProcessId].discountsList = action.payload;
    },
    checkAfterUpdate: (state, action) => {
      const activeProcessId = state.activeProcessId as number;
      const process = state.list[activeProcessId];

      if (
        process
        && process.status !== CNT.PROCESSES_STATUS_INITIALIZING
        && process.status !== CNT.PROCESSES_STATUS_COMPLETE
      ) {
        state.list[activeProcessId] = {
          ...process,
          status: CNT.PROCESSES_STATUS_INITIALIZING,
          discountType: null,
          discountsList: null,
          client: null
        }
      }
    },
    resetProcess: (state, action) => {
      const activeProcessId = state.activeProcessId as number;
      const process = state.list[activeProcessId];

      if (activeProcessId) {
        state.list[activeProcessId] = {
          ...process,
          status: CNT.PROCESSES_STATUS_INITIALIZING,
          discountType: null,
          discountsList: null,
          client: null
        }
      }
    }
  }
});

export const {
  initProcess,
  setStatus,
  setClient,
  setDiscountType,
  setDiscountsList,
  setActiveProcessId,
  checkAfterUpdate,
  resetProcess
} = processes.actions;

export default processes.reducer;
