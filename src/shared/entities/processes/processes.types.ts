import * as CNT from './processes.constants';

import * as C_TPS from '../client/client.types';
import * as A_TPS from '../abkaDiscounts/abkaDiscounts.types';

export type ProcessesState = {
  activeProcessId: number | null;
  list: ProcessesList;
};

export type ProcessesList = {
  [key: string]: {
    id: number;
    status: ProcessesStatus;
    client: C_TPS.ClientNormalizedData | null;
    discountType: A_TPS.AbkaDiscountType | null;
    discountsList: A_TPS.AbkaDiscountsNormalizedDiscount[] | null;
    calculationTime: number;
  }
}

export type ProcessesStatus = (
  typeof CNT.PROCESSES_STATUS_INITIALIZING
  | typeof CNT.PROCESSES_STATUS_EMPLOYEE_REGISTER
  | typeof CNT.PROCESSES_STATUS_EMPLOYEE_CONFIRM
  | typeof CNT.PROCESSES_STATUS_EMPLOYEE_BLOCKED
  | typeof CNT.PROCESSES_STATUS_CLIENT_IDENTIFY
  | typeof CNT.PROCESSES_STATUS_CLIENT_CONFIRM_ACTIVATION
  | typeof CNT.PROCESSES_STATUS_CLIENT_PROFILE
  | typeof CNT.PROCESSES_STATUS_CONFIRM_OPERATION
  | typeof CNT.PROCESSES_STATUS_COMPLETE
  | typeof CNT.PROCESSES_STATUS_CLOSED
  | typeof CNT.PROCESSES_STATUS_ERROR
);
