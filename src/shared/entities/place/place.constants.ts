export const PLACE_STORAGE_KEY = 'abka-place';

export const PLACE_STATUS_CHECK_IN = 'check-in';
export const PLACE_STATUS_ACTIVE = 'active';
export const PLACE_STATUS_NOT_REGISTERED = 'not-registered';
export const PLACE_STATUS_ERROR = 'check-in-error';

export const PLACE_CHECK_TIMEOUT = 2000;
