import { createSlice } from '@reduxjs/toolkit';

import * as CNT from './place.constants';
import * as TPS from './place.types';

const initialState: TPS.PlaceState = {
  status: CNT.PLACE_STATUS_CHECK_IN,
  data: null
}

const placeSlice = createSlice({
  name: 'place',
  initialState,
  reducers: {
    placeSave: (state, action) => {
      const { status, data } = action.payload;

      state.status = status;
      state.data = data;
    }
  }
});

export const { placeSave } = placeSlice.actions;

export default placeSlice.reducer;
