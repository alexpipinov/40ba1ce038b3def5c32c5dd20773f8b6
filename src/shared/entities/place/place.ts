import * as CNT from './place.constants';
import * as TPS from './place.types';
import { placeSave } from './place.state';

import * as A_API_CNT from '../../../shared/lib/api/abka/abka.api.constants';

import store from '../../store/configureStore';

import { callAbKa } from '../../lib/api/abka/abka.api.call';

export const placeGetState: TPS.PlaceGetState = async () => {
  const placeState = await chrome.storage.local.get([CNT.PLACE_STORAGE_KEY]);

  return placeState[CNT.PLACE_STORAGE_KEY];
}

export const placeWatchStateReady: TPS.PlaceWatchReady = () => {
  return new Promise((resolve) => {
    const interval = setInterval(async () => {
      const placeState = await placeGetState();

      if (placeState) {
        clearInterval(interval);
        resolve(placeState);
      }
    }, CNT.PLACE_CHECK_TIMEOUT);
  });
}

export const placeGetStatus: TPS.PlaceGetStatus = async () => {
  const placeState = await placeGetState() || await placeWatchStateReady();

  return placeState.status;
}

export const placeCheckIn = async () => {
  // Yes, it's a correct endpoint. API doesn't have a special method to check place.
  // We should check response 470 code for it.
  const response = await callAbKa('endpoint');

  switch (response.status) {
    case A_API_CNT.RESPONSE_STATUS_SUCCESS: {
      const data = response.data;
      const { complex, codeRepeatTimeout, codeTimeout } = data;
      const { servicePhoneNumber, rate } = complex;

      placeSync(CNT.PLACE_STATUS_ACTIVE, rate, servicePhoneNumber, codeTimeout, codeRepeatTimeout);
      break;
    }
    case A_API_CNT.RESPONSE_STATUS_PLACE_NOT_REGISTERED:
    case A_API_CNT.RESPONSE_STATUS_INTERNAL_SERVER_ERROR:
      placeSync(CNT.PLACE_STATUS_NOT_REGISTERED);
      break;
    case A_API_CNT.RESPONSE_STATUS_NOT_FOUND: {
      const data = response.data;
      const { complex, codeTimeout, codeRepeatTimeout } = data;
      const { rate, servicePhoneNumber } = complex;

      placeSync(CNT.PLACE_STATUS_ACTIVE, rate, servicePhoneNumber, codeTimeout, codeRepeatTimeout);
      break;
    }
    default: {
      try {
        placeSync(CNT.PLACE_STATUS_ACTIVE);
      } catch (error) {
        placeSync(CNT.PLACE_STATUS_ERROR)
      }
      break;
    }
  }
}

export const placeSync: TPS.PlaceSync = async (
  status,
  rate,
  placePhoneNumber = '',
  initialCodeTimeout = 0,
  repeatCodeTimeout = 0,
) => {
  const place: TPS.Place = {
    placePhoneNumber,
    initialCodeTimeout,
    repeatCodeTimeout,
    rate
  };
  const data = { status, data: place };

  store.dispatch(placeSave(data));
  await chrome.storage.local.set({[CNT.PLACE_STORAGE_KEY]: data});
}
