import * as CNT from './place.constants';

export interface PlaceState {
  status: PlaceStatus;
  data: Place | null;
}

export type PlaceStatus =
  typeof CNT.PLACE_STATUS_NOT_REGISTERED
  | typeof CNT.PLACE_STATUS_ACTIVE
  | typeof CNT.PLACE_STATUS_CHECK_IN
  | typeof CNT.PLACE_STATUS_ERROR;

export type PlacePhoneNumber = string;
export type PlaceInitialCodeTimeout = number;
export type PlaceRepeatCodeTimeout = number;

export interface Place {
  placePhoneNumber: PlacePhoneNumber;
  initialCodeTimeout: PlaceInitialCodeTimeout;
  repeatCodeTimeout: PlaceRepeatCodeTimeout;
  rate: PlaceRate;
}

export type PlaceRate = 'paid' | 'paid_plus' | 'free' | undefined;

export type PlaceGetState = () => Promise<PlaceState>;
export type PlaceGetStatus = () => Promise<PlaceStatus>;
export type PlaceWatchReady = () => Promise<PlaceState>;

export type PlaceSync = (
  status: PlaceStatus,
  rate?: PlaceRate,
  placePhoneNumber?: PlacePhoneNumber,
  initialCodeTimeout?: PlaceInitialCodeTimeout,
  repeatCodeTimeout?: PlaceRepeatCodeTimeout,
) => void;
