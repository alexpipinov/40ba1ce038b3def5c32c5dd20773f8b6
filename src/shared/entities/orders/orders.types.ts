export type OrdersState = {
  list: {
    [key: string]: Order;
  }
}

export interface Order {
  id: number;
  staff_id: number;
  datetime: string;
  client: {
    name: string;
    phone: string;
  }
  seance_length: number;
  salon_id: number;
  company_id: number;
  attendance: 0 | 1 | 2;
  confirmed: 1 | 0;
  paid_full: 1 | 0;
  comment: string;
  services: OrderYClientsService[];
  total: number;
  calculationTime: number;
}

export interface OrderYClientsService {
  amount: number;
  cost: number;
  cost_per_unit: number;
  cost_to_pay: number;
  discount: number;
  first_cost: number;
  id: number;
  manual_cost: number;
  title: string;
}

export interface OrderYClientsUpdateService {
  id: number;
  first_cost: number;
  discount: number;
  cost: number;
}

export interface OrderAbkaDiscounts {
  cashback: number;
  discount: number;
  positions: OrderAbKaDiscountPosition[];
  total: number;
}

export interface OrderAbKaDiscountPosition {
  cashback: number;
  count: number;
  discount: number;
  id: number;
  name: string;
  price: number;
  total: number;
}

export type OrderInfo = (recordId: string | number) => Promise<Order | null>;
export type OrderApplyDiscounts = (abkaDiscounts: OrderAbkaDiscounts) => Promise<ApiRequestNormalizedResponse>;
export type OrderConvertAbkaPositionsToServices = (positions: OrderAbKaDiscountPosition[]) => OrderYClientsUpdateService[];
