import * as TPS from './orders.types';
import * as LIB from './orders.lib';

import store from '../../store/configureStore';

import { callYClients } from "../../lib/api/yclients/yclients.api.call";

import { yClientsGetState } from "../yclients/yclients";

const getOrderFromState = () => {
  const { processes, orders } = store.getState();
  const activeProcessId = processes.activeProcessId as number;

  return orders.list[activeProcessId];
}

export const orderInfo: TPS.OrderInfo = async (recordId) => {
  const { salonId } = await yClientsGetState();
  const orderResponse = await callYClients(`record/${salonId}/${recordId}`);

  if (orderResponse.success) {
    return orderResponse.data;
  }

  return null;
}

export const orderApplyDiscounts: TPS.OrderApplyDiscounts = async (abkaDiscounts) => {
  const order = getOrderFromState();

  if (!order) {
    throw new Error('Отсутствуют необходимые данные. Обновите страницу и попробуйте снова');
  }

  const { id, company_id } = order;
  const recordResponse = await callYClients(`record/${company_id}/${id}`, 'PUT', {
      ...order,
      attendance: 1,
      services: LIB.convertAbkaPositionsToServices(abkaDiscounts.positions),
      comment: `ABKA_${id}`
    }
  );

  if (recordResponse.success) {
    orderUpdateCommentNode(`ABKA_${id}`);
    await orderUpdateNodes();
  }

  return recordResponse;
}

export const orderIsPaidWithLoyalty = async () => {
  const { processes } = store.getState();
  const { activeProcessId } = processes;

  if (!activeProcessId) {
    return false;
  }

  try {
    const yClientsOrder = await orderInfo(activeProcessId);

    if (!yClientsOrder) {
      return false;
    }

    const { paid_full, comment } = yClientsOrder;

    return !!(+paid_full && comment.includes('ABKA'));
  } catch (error) {
    return false;
  }
}

const orderUpdateNodes = async () => {
  const visitTab: HTMLElement | null = document.querySelector('.visit_tab');
  const paymentsTab: HTMLElement | null = document.querySelector('.payments_tab_2');

  return new Promise((resolve) => {
    if (visitTab) {
      visitTab.click();
    }

    setTimeout(() => {
      if (paymentsTab) {
        paymentsTab.click();
      }

      resolve(true);
    }, 500);
  });
}

const orderUpdateCommentNode = (comment: string) => {
  const commentNode = <HTMLInputElement>document.getElementById('rec_comment');

  if (!commentNode) {
    return;
  }

  commentNode.value = `${comment} \n ${commentNode.value}`;
}
