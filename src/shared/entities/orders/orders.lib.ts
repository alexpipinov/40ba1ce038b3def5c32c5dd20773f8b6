import * as TPS from './orders.types';

export const convertAbkaPositionsToServices: TPS.OrderConvertAbkaPositionsToServices = (
  positions
) => {
  return positions.map((position: TPS.OrderAbKaDiscountPosition) => ({
    id: position.id,
    first_cost: position.price,
    discount: position.discount,
    cost: position.total
  }));
}
