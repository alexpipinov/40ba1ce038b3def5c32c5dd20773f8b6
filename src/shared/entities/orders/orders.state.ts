import { createSlice } from '@reduxjs/toolkit';

import * as TPS from './orders.types';

const initialState: TPS.OrdersState = {
  list: {}
};

const ordersSlice = createSlice({
  name: 'orders',
  initialState,
  reducers: {
    setOrder: (state, action) => {
      const order = action.payload;

      state.list[order.id] = order;
    }
  }
});

export const { setOrder } = ordersSlice.actions;

export default ordersSlice.reducer;
