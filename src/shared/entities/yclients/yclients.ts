import * as CNT from './yclients.constants';
import * as TPS from './yclients.types';

import { callYClients } from '../../lib/api/yclients/yclients.api.call';

export const yClientsSyncData: TPS.YClientsSyncData = (data) => {
  return chrome.storage.local.set({[CNT.YCLIENTS_STORAGE_KEY]: data});
}

export const yClientsGetState: TPS.YClientsGetState = async () => {
  const state = await chrome.storage.local.get(CNT.YCLIENTS_STORAGE_KEY);

  return state[CNT.YCLIENTS_STORAGE_KEY];
}

export const yClientsGetPlaceInfo: TPS.YClientsGetPlaceInfo = async () => {
  try {
    const yClientsState = await yClientsGetState();

    if (!yClientsState) {
      return null;
    }

    const response = await callYClients(`company/${yClientsState.salonId}`);

    if (response.success) {
      const { id, city, address, phone, coordinate_lat, coordinate_lon } = response.data;

      return {
        id,
        city: city || '-',
        address: address || '-',
        phone: phone || '-',
        lat: coordinate_lat || '-',
        long: coordinate_lon || '-'
      };
    }

    return null;
  } catch (error) {
    console.log(['CATCH: GET COMPANY INFO'], error);
    return null;
  }
}
