export interface YClientsSessionData {
  uid: number;
  salonId: number;
  reloadId: number;
  token: string;
}

export interface YClientsPlaceInfo {
  id: number;
  city: string;
  address: string;
  phone: string;
  lat: string | number;
  long: string | number;
}

export type YClientsPlace = YClientsPlaceInfo | null;

export type YClientsSyncData = (data: YClientsSessionData) => void;
export type YClientsGetState = () => Promise<YClientsSessionData>;
export type YClientsGetPlaceInfo = () => Promise<YClientsPlace>;
