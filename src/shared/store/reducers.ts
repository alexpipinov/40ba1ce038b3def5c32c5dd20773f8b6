import { combineReducers } from "@reduxjs/toolkit";

import employeesReducer from "../entities/employees/employees.state";
import processesReducer from "../entities/processes/processes.state";
import placeReducer from "../entities/place/place.state";
import ordersReducer from "../entities/orders/orders.state";

const rootReducer = combineReducers({
  employees: employeesReducer,
  processes: processesReducer,
  orders: ordersReducer,
  place: placeReducer,
});

export default rootReducer;
