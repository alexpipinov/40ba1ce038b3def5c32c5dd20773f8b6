import { applyMiddleware } from 'redux';
import { createStore } from '@reduxjs/toolkit'
import { logger } from 'redux-logger';

import reducers from './reducers';

const middlewares = [];

if (NODE_ENV === 'development') {
  middlewares.push(logger);
}

const store = createStore(reducers, applyMiddleware(...middlewares));

export const { dispatch } = store;

export default store;
