import * as OP_TPS from '../../../content-scripts/screens/Order/components/OrderProcessing/OrderProcessing.types';

export interface EmployeeRegisterProps {
  context: 'admin' | 'staff';
  masterId: number;
  stepName: OP_TPS.OrderProcessingSteps;
  completeStep: (step: string) => void;
}
