import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as TPS from './EmployeeRegister.types';
import * as F_TPS from "../Form/Form.types";
import * as EMP_TPS from "../../entities/employees/employees.types";

import Form from "../Form";

import { isValidName, isValidPhone } from "../../lib/validation/validation";
import cleanPhoneInput from "../../lib/cleanPhoneInput/cleanPhoneInput";

import { RootState } from '../../store/store.types';

import { employeeGetYClientsAdminInfo, employeeGetYClientsStaffInfo, employeeRegister } from "../../entities/employees/employees";
import { employeeUpdateData } from '../../entities/employees/employees.state';


const EmployeeRegister: React.FC<TPS.EmployeeRegisterProps> = (props) => {
  const { context, masterId, stepName, completeStep } = props;

  const dispatch = useDispatch();

  const employees = useSelector((state: RootState) => state.employees);
  const employee = employees[masterId];

  useEffect(() => {
    if (context === 'admin') {
      employeeGetYClientsAdminInfo().then(info => {
        dispatch(employeeUpdateData({ uid: masterId, data: info }));
      });
    } else if (masterId) {
      employeeGetYClientsStaffInfo(masterId).then(info => {
        dispatch(employeeUpdateData({ uid: masterId, data: info }))
      });
    }
  }, []);


  const register = useCallback(async (data: EMP_TPS.EmployeeRegisterData) => {
    const response = await employeeRegister(data, masterId);

    if (response.success) {
      const employeeData = {
        uid: masterId,
        data: {
          phone: data.phone,
          name: data.name
        }
      };

      dispatch(employeeUpdateData(employeeData));
      completeStep(stepName);
    }

    return response;
  }, [employees]);

  const generateFieldsForRegisterForm = useCallback(() => {
    const registerFields: F_TPS.FormFields[] = [
      {
        type: 'hidden',
        name: 'method',
        defaultValue: 'sms',
        required: true
      },
      {
        type: 'text',
        name: 'name',
        placeholder: 'Имя Фамилия',
        defaultValue: employee?.data?.name,
        customValidator: {
          message: 'Введите имя и фамилию через пробел',
          method: isValidName
        },
        required: true
      },
      {
        type: 'mask',
        name: 'phone',
        mask: '+7 (999) 999-99-99',
        placeholder: 'Телефон сотрудника',
        defaultValue: employee?.data?.phone,
        customValidator: {
          message: 'Некорректный телефон',
          method: isValidPhone
        },
        normalizeInput: cleanPhoneInput,
        required: true
      }
    ];

    return registerFields;
  }, [employees]);

  if (!employee || !employee.data) {
    return null;
  }

  return (
    <Form
      key="employeeRegisterForm"
      descr={`Для работы в рамках данного филиала необходимо зарегистрировать сотрудника <strong>${employee?.data?.name || ''}</strong> в системе «АбсолютКарта»`}
      fields={generateFieldsForRegisterForm()}
      submitLabel="Зарегистрировать"
      onSubmit={register}
    />
  );
}

export default EmployeeRegister;
