import React, { useCallback } from 'react';

import * as TPS from './ClientIdentify.types';
import * as F_TPS from '../Form/Form.types';

import { callAbKa } from '../../lib/api/abka/abka.api.call';

import { isValidPhone } from '../../lib/validation/validation';
import cleanPhoneInput from '../../lib/cleanPhoneInput';

import { clientIdentify } from '../../entities/client/client';

import Form from '../Form';

const ClientIdentify: React.FC<TPS.ClientIdentifyProps> = (props) => {
  const { masterId, phone, stepName, completeStep } = props;

  const generateIdentifyFields = useCallback(() => {
    const identifyFields: F_TPS.FormFields[] = [
      {
        type: 'mask',
        name: 'phone',
        mask: '+7 (999) 999-99-99',
        placeholder: 'Телефон клиента',
        defaultValue: phone,
        customValidator: {
          message: 'Некорректный телефон',
          method: isValidPhone
        },
        normalizeInput: cleanPhoneInput,
        required: true
      }
    ];

    return identifyFields;
  }, []);

  const identify = useCallback(async (data) => {
    const response = await clientIdentify(masterId, data.phone);
    const isRegRetryRequest = response.status === 424;

    if (!response.success && !isRegRetryRequest) {
      if (!isRegRetryRequest) {
        await activate();
      }
    }

    completeStep(stepName, response.success);

    return response;
  }, [masterId]);

  const activate = async () => {
    const data = { uid: masterId, data: {
      userExtId: phone,
      notify: 1
    }};

    return callAbKa('endpoint', data);
  }

  return (
    <Form
      key="clientIdentifyForm"
      fields={generateIdentifyFields()}
      descr="Введите номер телефона клиента"
      submitLabel="Продолжить"
      onSubmit={identify}
    />
  );
}

export default ClientIdentify;
