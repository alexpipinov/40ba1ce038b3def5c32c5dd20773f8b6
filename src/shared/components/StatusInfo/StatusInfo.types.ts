export interface StatusInfoProps {
  text: string;
  title?: string;
  type?: 'info' | 'success' | 'error';
}
