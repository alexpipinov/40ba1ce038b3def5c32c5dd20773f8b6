import React from 'react';
import classNames from 'classnames';

import * as TPS from './StatusInfo.types';

import './StatusInfo.styles.scss';

const StatusInfo: React.FC<TPS.StatusInfoProps> = (props) => {
  const { title, text, type } = props;

  const titleStyles = classNames('b-status-info__title', {
    '_info': type === 'info',
    '_success': type === 'success',
    '_error': type === 'error'
  });

  return (
    <div className="b-status-info">
      {!title ? null :
        <div className={titleStyles}>
          {title}
        </div>}

        <div className="b-status-info__text">
          {text}
        </div>
    </div>
  );
}

StatusInfo.defaultProps = {
  type: 'info'
}

export default StatusInfo;
