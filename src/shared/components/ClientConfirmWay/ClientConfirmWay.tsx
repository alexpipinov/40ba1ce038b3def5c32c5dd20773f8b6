import React, { useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';

import * as TPS from './ClientConfirmWay.types';

import { RootState } from '../../store/store.types';

import { clientSendAppInstallLink } from '../../entities/client/client';
import formatPhoneNumber  from '../../lib/formatPhoneNumber';

import { useProcess } from '../../hooks';

import InlineLink from '../InlineLink';
import Button from '../Button';

import './ClientConfirmWay.styles.scss';

const ClientConfirmWay: React.FC<TPS.ClientConfirmWayProps> = (props) => {
  const [isMounted, markAsMounted] = useState(false);
  const [isChecking, setChecking]  = useState(false);

  const { masterId, checkConfirmation } = props;

  const process = useProcess();
  const place = useSelector((state: RootState) => state.place);
  const client = process.client;

  useEffect(() => {
    markAsMounted(true);

    return () => markAsMounted(false);
  }, []);

  const renderConfirmWayInfo = () => {
    const placeData = place.data;

    if (!client || !placeData) {
      return null;
    }

    if (client.interactionType === 'reduced') {
      const phoneData = placeData.placePhoneNumber.toString() || '';

      const [number, extension] = phoneData.split(',');
      const phone = `${formatPhoneNumber(number)}${extension ? `, доб.${extension}`: ''}`;


      return `Клиенту необходимо подтвердить операцию звонком на номер телефона ${phone}`;
    }

    if (client.appInstalled) {
      return 'Клиенту направлен PUSH для подтверждения операции в приложении';
    }

    if (placeData && placeData.rate === 'paid_plus') {
      return 'Клиенту направлено SMS с ссылкой для подтверждения операции на сайте';
    } else {
      return 'Для подтверждения операции клиенту необходимо установить приложение';
    }
  }

  const handleCheck = useCallback(async (e) => {
    e.preventDefault();

    setChecking(true);

    await checkConfirmation();

    if (isMounted) {
      setChecking(false);
    }
  }, [isMounted]);


  const sendAppInstallLink = useCallback(() => {
    if (!client) {
      return;
    }

    return clientSendAppInstallLink(masterId, client.phone);
  }, []);

  const renderLink = () => {
    if (!client || !place.data) {
      return null;
    }

    const { interactionType, appInstalled } = client;

    if (interactionType === 'full' && appInstalled === 0 && place.data.rate !== 'paid_plus') {
      return (
        <InlineLink
          key="appInstallLink"
          label="отправить ссылку"
          handler={sendAppInstallLink}
        />
      );
    }

    return null;
  }

  if (!client) {
    return null;
  }

  return (
    <div className="b-client-confirm-way">
      {renderConfirmWayInfo()}
      {' '}
      {renderLink()}

      <div className="b-client-confirm-way__check-btn-wrap">
        <Button
          label="Проверить подтверждение"
          bgColor="green"
          size="big"
          isFetching={isChecking}
          handlerSubmit={handleCheck}
        />
      </div>
    </div>
  );
}

export default ClientConfirmWay;
