export interface ClientConfirmWayProps {
  masterId: number;
  checkConfirmation: () => Promise<ApiRequestNormalizedResponse>
}
