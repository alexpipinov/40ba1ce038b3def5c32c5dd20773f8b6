export const VALUE_TYPE_CURRENCY = 'currency';
export const VALUE_TYPE_PERCENT = 'percent';
export const VALUE_TYPE_TEXT = 'text';
export const VALUE_TYPE_FUNC = 'func';
