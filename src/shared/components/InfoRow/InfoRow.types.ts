import * as CNT from './InfoRow.constants';

export interface InfoRowProps {
  label: string;
  value: InfoRowValue;
  valueType: InfoRowValueType;
  note?: string;
  funcLabel?: string;
  highlightError?: boolean;
  highlightForSuccess?: boolean;
}

export interface InfoRowDefaultProps {
  funcLabel: string;
  highlightError: boolean;
  highlightForSuccess: boolean;
}

export type InfoRowValue = string | number | Function;
export type InfoRowValueType = (
  typeof CNT.VALUE_TYPE_FUNC
  | typeof CNT.VALUE_TYPE_CURRENCY
  | typeof CNT.VALUE_TYPE_PERCENT
  | typeof CNT.VALUE_TYPE_TEXT
);
