import React from 'react';
import classNames from 'classnames';

import * as CNT from './InfoRow.constants';
import * as TPS from './InfoRow.types';

import formatCurrency from '../../lib/formatCurrency';

import './InfoRow.styles.scss';

const defaultProps: TPS.InfoRowDefaultProps = {
  funcLabel: 'изменить',
  highlightError: false,
  highlightForSuccess: false
};

function renderValue(value: TPS.InfoRowValue, valueType: TPS.InfoRowValueType, label?: string) {
  switch (valueType) {
    case CNT.VALUE_TYPE_FUNC:
      return (
        <a
          href="#"
          className="b-info-row__link"
          onClick={value as React.MouseEventHandler}
        >
          {label}
        </a>
      );
    case CNT.VALUE_TYPE_CURRENCY:
      return `${formatCurrency(value as number)} ₽`;
    case CNT.VALUE_TYPE_PERCENT:
      return `${value}%`;
    default:
      return value;
  }
}

const InfoRow: React.FC<TPS.InfoRowProps>  = (props) => {
  const { label, note, value, valueType, funcLabel, highlightError, highlightForSuccess } = props;

  const ctx = classNames('b-info-row', {
    '_with-error': highlightError,
    '_for-success': highlightForSuccess
  });

  return (
    <div className={ctx}>
      <div className="b-info-row__descr-wrap">
        <div className="b-info-row__label">
          {label}
        </div>
        {!note ? null :
          <div className="b-info-row__note">{note}</div>}
      </div>

      <div className="b-info-row__value">
        { highlightError && valueType === 'currency' ? '+ ' : null }
        { renderValue(value, valueType, funcLabel) }
      </div>
    </div>
  );
}


InfoRow.defaultProps = defaultProps;

export default InfoRow;
