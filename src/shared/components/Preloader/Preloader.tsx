import React from 'react';

import * as TPS from './Preloader.types';

import './Preloader.styles.scss';

const Preloader: React.FC<TPS.PreloaderProps> = (props) => {
  const { color } = props;

  return (
    <div className="b-preloader">
      <div className={`b-circle__a _${color}`} />
      <div className={`b-circle__b _${color}`} />
    </div>
  );
}

Preloader.defaultProps = {
  color: 'white'
};

export default Preloader;


