export interface PreloaderProps {
  color?: string;
  size?: 'auto' | number;
}
