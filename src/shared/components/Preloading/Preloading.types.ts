export interface PreloadingProps {
  size?: number;
  color?: string;
}
