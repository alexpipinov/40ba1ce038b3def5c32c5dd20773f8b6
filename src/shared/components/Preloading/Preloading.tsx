import React from 'react';

import * as TPS from './Preloading.types';

import Preloader from '../Preloader';

import './Preloading.styles.scss';

const Preloading: React.FC<TPS.PreloadingProps> = (props) => {
  const { size, color } = props;

  return (
    <div className="b-preloading">
      <div
        className="b-preloading__preloader-wrap"
        style={{width: size, height: size}}
      >
        <Preloader color={color} />
      </div>
    </div>
  );
}

Preloading.defaultProps = {
  size: 20,
  color: 'green'
};

export default Preloading;
