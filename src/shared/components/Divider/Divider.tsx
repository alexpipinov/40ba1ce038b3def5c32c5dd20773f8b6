import React from 'react';

import './Divider.styles.scss';

const Divider = () => {
  return (
    <div className="b-divider"/>
  );
}

export default Divider;
