import React, { useCallback } from 'react';

import * as TPS from './ConfirmOperation.types';
import * as F_TPS from "../Form/Form.types";
import * as AD_TPS from '../../../shared/entities/abkaDiscounts/abkaDiscounts.types';

import ClientConfirmWay from '../ClientConfirmWay';
import ClientSendCode from '../ClientSendCode';
import Form from '../Form';
import Reset from '../Reset';

import { abkaDiscountsCheck, abkaDiscountsConfirm } from '../../entities/abkaDiscounts/abkaDiscounts';
import { orderApplyDiscounts } from '../../entities/orders/orders';

import './ConfirmOperation.styles.scss';

const ConfirmOperation: React.FC<TPS.ConfirmOperationProps> = (props) => {
  const { masterId, discountType, stepName, completeStep } = props;

  const generateFieldsForConfirmForm = useCallback(() => {
    const confirmFields: F_TPS.FormFields[] = [
      {
        type: 'mask',
        name: 'code',
        mask: '9999',
        placeholder: 'Введите код',
        required: true
      }
    ];

    return confirmFields;
  }, []);

  const checkConfirmation = useCallback(async () => {
    const result = await abkaDiscountsCheck(discountType);

    if (result.success) {
      const { bill, confirmation } = result.data;

      if (confirmation.isConfirmed) {
        await applyDiscounts(bill);
      }
    }

    return result;
  }, [discountType]);

  const confirmDiscounts = async (data: { code: string }) => {
    const result = await abkaDiscountsConfirm(discountType, data.code);

    if (result.success) {
      const { bill } = result.data;
      await applyDiscounts(bill);
    }

    return result;
  }

  const applyDiscounts = async (abkaDiscounts: AD_TPS.AbkaDiscounts) => {
    const applyResponse = await orderApplyDiscounts(abkaDiscounts);

    completeStep(stepName, applyResponse.success);
  }


  return (
    <div className="b-confirm-operation">
      <div className="b-confirm-operation__confirm-way-wrap">
        <ClientConfirmWay
          checkConfirmation={checkConfirmation}
          masterId={masterId}
        />
      </div>

      <div className="b-confirm-operation__send-code-wrap">
        <ClientSendCode
          discountType={discountType}
        />
      </div>

      <Form
        key="employeeSyncForm"
        fields={generateFieldsForConfirmForm()}
        submitLabel="Подтвердить"
        onSubmit={confirmDiscounts}
      />

      <Reset />
    </div>
  );
}

export default ConfirmOperation;
