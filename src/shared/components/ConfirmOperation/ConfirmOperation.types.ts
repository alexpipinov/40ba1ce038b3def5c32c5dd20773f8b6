import * as AD_TPS from '../../entities/abkaDiscounts/abkaDiscounts.types';

import * as CNT from "../../../content-scripts/screens/Order/components/OrderProcessing/OrderProcessing.constants";

export interface ConfirmOperationProps {
  discountType: AD_TPS.AbkaDiscountType;
  masterId: number;
  stepName: typeof CNT.ORDER_PROCESSING_STEP_CONFIRM_OPERATION;
  completeStep: (step?: string, success?: boolean) => void;
}
