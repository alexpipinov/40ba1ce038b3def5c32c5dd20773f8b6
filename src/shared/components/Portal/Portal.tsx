import React, { useState, useEffect, useCallback } from 'react';
import ReactDOM from 'react-dom';

import * as TPS from './Portal.types';

import '../../styles/index.scss';
import './Portal.styles.scss';

const Portal = (props: TPS.PortalProps) => {
  const [portalRoot] = useState(() => {
    const node = document.createElement('div')
    node.id = 'abka-portal-root';

    return node;
  });

  useEffect(() => {
    document.body.appendChild(portalRoot);
    return () => {
      document.body.removeChild(portalRoot)
    }
  }, []);

  const stopPropagation: React.MouseEventHandler = useCallback((e) => {
    e.stopPropagation();
  }, []);

  return ReactDOM.createPortal(
    <div className="b-abka-portal" onClick={stopPropagation}>
      <div className="b-abka-portal__content-wrap">
        {props.children}
      </div>
    </div>,
    portalRoot
  );
}

export default Portal;
