import React from 'react';

export interface PortalProps {
  children: React.ReactChild
}
