export interface InlineLinkProps {
  label: string;
  handler: Function;
}
