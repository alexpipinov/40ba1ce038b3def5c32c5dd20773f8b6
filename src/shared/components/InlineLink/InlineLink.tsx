import React, { useEffect, useState, useCallback } from 'react';

import * as TPS from './InlineLink.types';

import CircleArrowIcon from '../Icons/CircleArrowIcon';
import Preloader from '../Preloader';

import './InlineLink.styles.scss';

const InlineLink: React.FC<TPS.InlineLinkProps> = (props) => {
  const [ isFetching, setFetching ] = useState(false);
  const [ isMounted, markAsMounted ] = useState(false);
  const { label, handler } = props;

  useEffect(() => {
    markAsMounted(true);

    return () => {
      markAsMounted(false);
    }
  }, []);

  const handleClick = useCallback(async (e) => {
    e.preventDefault();

    setFetching(true);

    await handler();

    if (isMounted) {
      setFetching(false);
    }
  }, [isMounted]);

  return (
    <div className="b-inline-link">
      <a
        href="#"
        className="b-inline-link__link"
        onClick={handleClick}
      >
        <span className="b-inline-link__label-wrap">
          {label}
        </span>

        <span className="b-inline-link__icon-wrap">
          {isFetching
            ? <Preloader color="green" />
            : <CircleArrowIcon color="green" />}
        </span>
      </a>
    </div>
  );
}

export default InlineLink;
