import React, { useCallback } from 'react';

import * as TPS from './ClientConfirmActivation.types';
import * as F_TPS from '../Form/Form.types';

import InlineLink from "../InlineLink";
import Reset from '../Reset';

import { callAbKa } from '../../lib/api/abka/abka.api.call';

import { isValidPhone } from '../../lib/validation/validation';
import cleanPhoneInput from '../../lib/cleanPhoneInput';

import { clientIdentify } from '../../entities/client/client';

import Form from '../Form';

import './ClientClnfirmActivation.styles.scss';

const ClientConfirmActivation: React.FC<TPS.ClientConfirmActivationProps> = (
  props
) => {
  const { masterId, phone, stepName, completeStep } = props;

  const generateConfirmActivationFields = useCallback(() => {
    const identifyFields: F_TPS.FormFields[] = [
      {
        type: 'mask',
        name: 'phone',
        mask: '+7 (999) 999-99-99',
        placeholder: 'Телефон клиента',
        customValidator: {
          message: 'Некорректный телефон',
          method: isValidPhone
        },
        normalizeInput: cleanPhoneInput,
        required: true
      }
    ];

    return identifyFields;
  }, []);

  const confirm = useCallback(async (data) => {
    const responseData = { uid: masterId, data: { userExtId: phone } };
    const response = await callAbKa('endpoint', responseData);

    if (response.success) {
      //Before complete fetch client data
      await clientIdentify(masterId, data.phone);
      completeStep(stepName);
    }

    return response;
  }, [masterId]);

  const checkConfirmation = async () => {
    const response = await clientIdentify(masterId, phone);

    if (response.success) {
      completeStep(stepName);
    }

    return response;
  }

  return (
    <div className="b-client-confirm-activation">
      <div className="b-client-confirm-activation__descr">
        Дождитесь подтверждения регистрации клиентом
        <InlineLink
          label="проверить подтверждение"
          handler={checkConfirmation}
        />, или введите повторно номер телефона
      </div>

      <Form
        key="clientConfirmActivation"
        fields={generateConfirmActivationFields()}
        onSubmit={confirm}
      />

      <Reset />
    </div>
  );
}

export default ClientConfirmActivation;
