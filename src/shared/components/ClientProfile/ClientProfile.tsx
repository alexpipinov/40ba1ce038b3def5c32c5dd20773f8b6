import React, { useEffect, useState, useCallback } from 'react';

import * as TPS from './ClientProfile.types';
import * as AD_TPS from '../../entities/abkaDiscounts/abkaDiscounts.types';

import Button from '../Button';
import InfoRow from '../InfoRow';
import InlineNotification from '../InlineNotification';
import Reset from '../Reset';

import { abkaDiscountsGetInfo, abkaDiscountsRequest } from '../../entities/abkaDiscounts/abkaDiscounts';
import { orderApplyDiscounts } from '../../entities/orders/orders';

import useOrder from '../../hooks/useOrder';
import useProcess from '../../hooks/useProcess';

import './ClientProfile.styles.scss';

const ClientProfile: React.FC<TPS.ClientProfileProps> = (props) => {
  const { stepName,  completeStep } = props;

  const [errors, setErrors] = useState<ErrorMessage[]>([]);
  const order = useOrder();
  const process = useProcess();

  useEffect(() => {
    if (order) {
      abkaDiscountsGetInfo();
    }
  }, []);

  const clientName = useCallback(() => {
    return order?.client.name || '';
  }, [order]);

  const setRequestErrors = (messages?: ErrorMessage[]) => {
    const info = messages || [{ message: 'Произошла ошибка. Отсутствуют данные о записи. Обновите страницу попробуйте снова!' }];

    setErrors(info);
  }

  const handleDiscountChoice = useCallback(async (e) => {
    e.preventDefault();
    const discountType = e.currentTarget.getAttribute('id');

    if (!order) {
      setRequestErrors();
      return;
    }

    await requestDiscounts(discountType)
  }, []);

  const requestDiscounts = useCallback(async (discountType) => {
    const { success, data, messages } = await abkaDiscountsRequest(discountType);
    const containsErrors = !success && messages.length;

    if (containsErrors) {
      setRequestErrors(messages);
      return;
    }

    if (success) {
      await applyDiscounts(data.bill)
    }

    completeStep(stepName, success);
  }, [order]);

  const applyDiscounts = async (abkaDiscounts: AD_TPS.AbkaDiscounts) => {
    await orderApplyDiscounts(abkaDiscounts);
  }

  if (!process || !process.discountsList) {
    return null;
  }

  if (!process.discountsList.length) {
    return (
      <div className="b-client-profile">
        <div className="b-client-profile__error-msg">
          Не удалось загрузить дисконты клиента. <br />
          Обратитесь в службу поддержки «АбсолютКарта»
        </div>
      </div>
    );
  }

  return (
    <div className="b-client-profile">
      {!clientName() ? null :
        <InfoRow
          label="Клиент:"
          value={clientName()}
          valueType="text"
        />}

      {!errors.length ? null :
        <InlineNotification
          title="Произошла ошибка"
          type="errors"
          descr="Обновите страницу и попробуйте снова. Если ошибка повторится, обратитесь в поддержку АбсолютКарта"
          items={errors}
        />}

      <div className="b-client-profile__btns-wrap">
        {process.discountsList.map(discount => {
          return (
            <div
              key={`${discount.discountType}Button`}
              className="b-client-profile__btn-wrap"
            >
              <Button
                id={discount.discountType}
                label={discount.description.label}
                bgColor={discount.discountType === 'payFromBill' ? 'blue' : 'green'}
                type="stroked"
                size="super-big"
                isFetching={process.discountType === discount.discountType}
                handlerSubmit={handleDiscountChoice}
              />
            </div>
          );
        })}
      </div>

      <Reset />
    </div>
  );
}

export default ClientProfile;
