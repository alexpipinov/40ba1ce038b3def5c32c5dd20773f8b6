import * as CNT from "../../../content-scripts/screens/Order/components/OrderProcessing/OrderProcessing.constants";

export interface ClientProfileProps {
  stepName: typeof CNT.ORDER_PROCESSING_STEP_CLIENT_PROFILE;
  completeStep: (step?: string, success?: boolean) => void;
}
