import * as A_TPS from '../../../shared/entities/abkaDiscounts/abkaDiscounts.types';

export interface ClientSendCodeProps {
  discountType: A_TPS.AbkaDiscountType;
}
