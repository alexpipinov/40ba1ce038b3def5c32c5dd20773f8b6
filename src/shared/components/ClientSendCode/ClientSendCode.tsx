import React, { useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';

import * as TPS from './ClientSendCode.types';

import { RootState } from '../../store/store.types';

import InlineLink from '../InlineLink';

import { abkaDiscountsSendCode } from '../../entities/abkaDiscounts/abkaDiscounts';

import './ClientSendCode.styles.scss';

const ClientSendCode: React.FC<TPS.ClientSendCodeProps> = (props) => {
  const place = useSelector((state: RootState) => state.place);

  const initialCodeTimeout = place?.data?.initialCodeTimeout || 0;
  const repeatCodeTimeout = place?.data?.repeatCodeTimeout || 0;

  const [ initialInterval, setInitialInterval ] = useState(initialCodeTimeout);
  const [ repeatInterval, setRepeatInterval ] = useState(0);

  if (!place.data) {
    return null;
  }

  useEffect(() => {
    const timer = runTimer(initialInterval, setInitialInterval);

    return () => clearInterval(timer);
  }, [initialInterval]);

  useEffect(() => {
    const timer = runTimer(repeatInterval, setRepeatInterval);

    return () => clearInterval(timer);
  }, [repeatInterval]);

  const runTimer = (
    curInterval: number,
    dispatchInterval: React.Dispatch<React.SetStateAction<number>>
  ) => {
    const timer = setInterval(() => {
      if (!curInterval) {
        clearInterval(timer);
      } else {
        dispatchInterval(curInterval - 1);
      }
    }, 1000);

    return timer;
  }

  const sendCodeHandler = useCallback(async () => {
    const result = await abkaDiscountsSendCode(props.discountType);

    if (result.success && repeatCodeTimeout) {
      setRepeatInterval(repeatCodeTimeout);
    }

    return result;
  }, [place]);

  if (initialInterval) {
    return (
      <div>
        Возникли проблемы? Через {initialInterval} сек. вы сможете направить клиенту код подтверждения
      </div>
    );
  } else if (repeatInterval) {
    return (
      <div>
        Клиенту направлен код в SMS. Повтор возможен через {repeatInterval} сек.
      </div>
    );
  } else {
    return (
      <InlineLink
        label="Отправить код повторно"
        handler={sendCodeHandler}
      />
    );
  }
}

export default ClientSendCode;
