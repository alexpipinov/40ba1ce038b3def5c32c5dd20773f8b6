import React from 'react';

import * as TPS from './CircleArrowIcon.types';

import './CircleArrowIcon.scss';

const CircleArrowIcon: React.FC<TPS.CircleArrowIconProps> = (props) => {
  const iconStyles = `b-circle-arrow-icon b-circle-arrow-icon_color_${props.color}`;

  return (
    <svg
      className={iconStyles}
      version="1.1"
      id="Layer_1"
      x="0px"
      y="0px"
      width="18px"
      height="18px"
      viewBox="0 0 18 18"
      enableBackground="new 0 0 18 18"
    >
      <path
        className="b-circle-arrow-icon__path"
        d="M15.187,2.811C13.535,1.159,11.338,0.25,9.002,0.25c-2.337,0-4.535,0.91-6.188,2.562
      c-3.412,3.413-3.412,8.965,0,12.376c1.653,1.652,3.851,2.562,6.188,2.562s4.534-0.91,6.187-2.562
      C18.598,11.776,18.598,6.223,15.187,2.811z M14.127,14.127c-1.369,1.369-3.19,2.123-5.126,2.123
      c-1.937,0-3.757-0.754-5.127-2.123
      c-2.827-2.827-2.827-7.428,0-10.255C5.244,2.504,7.065,1.75,9.002,1.75
      c1.936,0,3.756,0.753,5.125,2.122
      C16.953,6.699,16.953,11.3,14.127,14.127z"
      />
      <polygon
        className="b-circle-arrow-icon__polygon"
        points="7.469,6.196 10.274,8.998 7.471,11.803 8.532,12.864 12.395,8.997 8.53,5.135"
      />
    </svg>
  );
}

CircleArrowIcon.defaultProps = {
  color: 'green'
};

export default CircleArrowIcon;
