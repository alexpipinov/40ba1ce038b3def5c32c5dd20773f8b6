export interface CircleArrowIconProps {
  color?: 'green' | 'black';
}
