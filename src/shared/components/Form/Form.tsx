import React, { useState, useEffect, useCallback, useRef } from 'react';
import { useForm } from 'react-hook-form';

import * as CNT from './Form.constants';
import * as TPS from './Form.types';

import MaskInput from './Fields/MaskInput';
import TextInput from './Fields/TextInput';
import HiddenInput from './Fields/HiddenInput';

import Button from '../../components/Button';

const Form: React.FC<TPS.FormProps> = (props) => {
  const { fields, descr, submitLabel, onSubmit } = props;

  const { register, formState, control, setError, handleSubmit } = useForm();
  const [isFetching, setFetching] = useState<boolean>(false);
  const requirementsRef = useRef<TPS.FormRequirements>();

  useEffect(() => {
    const result: TPS.FormRequirements = {};

    for (const field of fields) {
      result[field.name] = [
        !!field.required,
        field.customValidator
      ];
    }

    requirementsRef.current = result;
  }, []);

  const { errors } = formState;

  const setValidationErrors = (errors: ErrorMessage[]) => {
    errors.forEach(error => {
      if (error.field) {
        setError(error.field, { message: error.message })
      }
    });
  }

  const checkErrors: TPS.FormCheckErrors = (inputs) => {
    const errors: TPS.FormValidationError[] = [];
    const requirements = requirementsRef.current;

    if (!requirements) {
      return errors;
    }

    for (const [key, value] of Object.entries(inputs)) {
      const [required, customValidator] = requirements[key];

      if (required && !value) {
        errors.push({ field: key, message: 'Обязательное поле'});
      }

      if (customValidator) {
        const isValid = customValidator.method(value);

        if (!isValid) {
          errors.push({ field: key, message: customValidator.message || 'Некорректное значение'})
        }
      }
    }

    return errors;
  }

  const validateInputs: TPS.FormValidateInputs = (inputs) => {
    const errors = checkErrors(inputs);

    if (errors.length) {
      setValidationErrors(errors)
      return false;
    }

    return true;
  }

  const normalizeInputs: TPS.FormNormalizeInputs = (inputs => {
    for (const field of fields) {
      if (field.normalizeInput) {
        inputs[field.name] = field.normalizeInput(inputs[field.name]);
      }
    }
  });

  const submit = useCallback(async (data) => {
    setFetching(true);
    normalizeInputs(data);

    const isValidInputs = validateInputs(data);

    if (isValidInputs) {
      const response = await onSubmit(data);

      if (!response.success) {
        setValidationErrors(response.messages);
      }
    }

    setFetching(false);
  }, []);

  const renderField = (field: TPS.FormFields) => {
    switch (field.type) {
      case CNT.FORM_FIELD_TYPE_TEXT:
        return (
          <TextInput
            key={`field-${field.type}-${field.name}`}
            type={field.type}
            name={field.name}
            placeholder={field.placeholder}
            error={errors[field.name]}
            register={register}
            defaultValue={field.defaultValue}
            readOnly={field.readOnly}
            required={field.required}
          />
        );
      case CNT.FORM_FIELD_TYPE_MASK:
        return (
          <MaskInput
            key={`field-${field.type}-${field.name}`}
            type={field.type}
            name={field.name}
            mask={field.mask}
            placeholder={field.placeholder}
            autocomplete={field.autocomplete}
            control={control}
            register={register}
            error={errors[field.name]}
            defaultValue={field.defaultValue}
            readOnly={field.readOnly}
            required={field.required}
          />
        );
      case CNT.FORM_FIELD_TYPE_HIDDEN:
        return (
          <HiddenInput
            key={`field-${field.type}-${field.name}`}
            type={field.type}
            name={field.name}
            defaultValue={field.defaultValue}
            register={register}
            required={field.required}
          />
        );
      default:
        return null;
    }
  };

  return (
    <form
      className="b-form"
      onSubmit={handleSubmit(submit)}
    >
      {!descr ? null :
        <div
          className="b-form__descr"
          dangerouslySetInnerHTML={{__html: descr}}
        />}

      {fields.map((field: TPS.FormFields) => renderField(field))}

      <div className="b-form__btn-wrapper">
        <Button
          label={submitLabel}
          bgColor="green"
          type="stroked"
          size="big"
          isFetching={isFetching}
        />
      </div>
    </form>
  );
}

Form.defaultProps = {
  submitLabel: 'Подтвердить'
}

export default Form;
