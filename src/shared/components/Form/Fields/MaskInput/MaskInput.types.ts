import * as F_TPS  from '../../Form.types';

export interface MaskInputProps extends MaskInput {
  register: any;
  control: any;
  error?: string;
}

export interface MaskInput extends F_TPS.FormField {
  mask: string;
}
