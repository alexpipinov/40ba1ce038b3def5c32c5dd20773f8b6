import React from 'react';
import { Controller } from 'react-hook-form';
import InputElement from 'react-input-mask';

import * as TPS from './MaskInput.types'

import InputFieldWrapper from '../InputFieldWrapper';

const MaskInput = (props: TPS.MaskInputProps) => {
  const {
    name,
    label,
    placeholder,
    defaultValue,
    autocomplete,
    mask,
    required,
    readOnly,
    error,
    control,
  } = props;

  return (
    <InputFieldWrapper
      name={name}
      label={label}
      error={error}
      required={required}
    >
      <Controller
        control={control}
        name={name}
        defaultValue={defaultValue}
        rules={{
          required
        }}
        render={({ field }) => (
          <InputElement
            id={name}
            type="phone"
            name={name}
            inputRef={field.ref}
            defaultValue={defaultValue}
            readOnly={readOnly}
            className="b-form__input"
            onChange={val => field.onChange(val)}
            placeholder={placeholder}
            autoComplete={autocomplete}
            mask={mask}
          />
        )}
      />
    </InputFieldWrapper>
  );
}

MaskInput.defaultProps = {
  label: null,
  placeholder: null,
  defaultValue: null,
  disabled: false,
  autocomplete: 'off',
  error: null,
  onFocus: null
};

export default MaskInput;
