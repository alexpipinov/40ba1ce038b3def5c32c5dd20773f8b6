import React from 'react';

import * as TPS from './HiddenInput.types';

const HiddenInput = (props: TPS.HiddenInputProps) => {
  const { name, defaultValue, required, register } = props;

  return (
    <input
      id={name}
      type="hidden"
      defaultValue={defaultValue}
      {...register(name, {
        required
      })}
    />
  );
}

HiddenInput.defaultProps = {
  defaultValue: null,
  required: false,
  onFocus: null
};

export default HiddenInput;
