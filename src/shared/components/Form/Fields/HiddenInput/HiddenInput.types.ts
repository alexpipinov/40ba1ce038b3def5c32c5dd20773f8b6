import { FormField } from '../../Form.types';

export interface HiddenInputProps extends FormField {
  register: any;
}
