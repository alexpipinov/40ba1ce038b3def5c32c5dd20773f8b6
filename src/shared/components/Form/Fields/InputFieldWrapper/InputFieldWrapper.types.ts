import React from 'react';

export interface InputFieldWrapperProps {
  name: string;
  children: React.ReactChild;
  required?: boolean;
  label?: string;
  error?: string;
}
