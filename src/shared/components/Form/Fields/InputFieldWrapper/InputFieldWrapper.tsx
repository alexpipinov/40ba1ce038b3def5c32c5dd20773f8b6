import React from 'react';
import classNames from 'classnames';

import * as TPS from './InputFieldWrapper.types';

const InputFieldWrapper = (props: TPS.InputFieldWrapperProps) => {
  const { name, label, error, required, children } = props;

  function getWrapperStyles() {
    return (
      classNames('b-form__field-wrapper', {
      '_has-errors': error
      })
    );
  }

  function getErrorMsg(error: any) {
    return error.message || 'Некорректное значение';
  }

  return (
    <div className={getWrapperStyles()}>
      {!label ? null :
        <div className="b-form__label-box">
          <label
            htmlFor={name}
            className="b-form__label"
          >
            {label} {!required ? null : <sup className="b-form__req-asterisk">*</sup>}
          </label>
        </div>}

      {children}

      {!error ? null :
        <span className="b-form__error-msg">
          {getErrorMsg(error)}
        </span>}
    </div>
  );
}

InputFieldWrapper.defaultProps = {
  label: null,
  error: null,
  required: false,
  isFetching: false
};

export default InputFieldWrapper;
