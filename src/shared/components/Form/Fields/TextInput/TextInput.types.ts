import * as F_TPS from '../../Form.types';

export interface TextInputProps extends F_TPS.FormField {
  register: any;
  error?: string;
}
