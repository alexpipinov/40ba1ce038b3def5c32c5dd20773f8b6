import React from 'react';

import * as TPS from './TextInput.types';

import InputFieldWrapper from '../InputFieldWrapper';

const TextInput = (props: TPS.TextInputProps) => {
  const {
    name,
    label,
    placeholder,
    autocomplete,
    defaultValue,
    readOnly,
    error,
    required,
    register
  } = props;

  return (
    <InputFieldWrapper
      name={name}
      label={label}
      required={required}
      error={error}
    >
      <input
        id={name}
        type="text"
        className="b-form__input"
        placeholder={placeholder}
        defaultValue={defaultValue}
        autoComplete={autocomplete}
        readOnly={readOnly}
        {...register(name, {
          required
        })}
      />
    </InputFieldWrapper>
  );
}

TextInput.defaultProps = {
  placeholder: '',
  defaultValue: null,
  autocomplete: 'off',
  readOnly: false,
  required: false,
  error: '',
};

export default TextInput;
