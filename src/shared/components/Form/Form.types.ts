import * as CNT from './Form.constants';

export interface FormProps {
  fields: FormField[];
  onSubmit: Function
  descr?: string;
  submitLabel?: string;
}

export type FormFieldType = (
  typeof CNT.FORM_FIELD_TYPE_TEXT
  | typeof CNT.FORM_FIELD_TYPE_MASK
  | typeof CNT.FORM_FIELD_TYPE_HIDDEN
);

export type FormFields = (
  FormTextField
  | FormMaskField
  | FormHiddenField
);

export interface FormField {
  type: FormFieldType;
  name: string;
  label?: string;
  placeholder?: string;
  readOnly?: boolean;
  required?: boolean;
  autocomplete?: 'on' | 'off';
  defaultValue?: string | undefined;
  customValidator?: FormInputCustomValidator;
  normalizeInput?: Function
}

export interface FormTextField extends FormField {
  type: typeof CNT.FORM_FIELD_TYPE_TEXT;
}

export interface FormHiddenField extends FormField {
  type: typeof CNT.FORM_FIELD_TYPE_HIDDEN;
}

export interface FormMaskField extends FormField {
  type: typeof CNT.FORM_FIELD_TYPE_MASK;
  mask: string;
}

export interface FormInputCustomValidator {
  method: FormInputValidator;
  message?: string;
}

export type FormInputValidator = (input: string)  => boolean;

export type FormFieldRequirement = [
  boolean,
  FormInputCustomValidator | undefined
];

export interface FormRequirements {
  [key: string]: FormFieldRequirement
}

export interface FormInputs {
  [key: string]: string;
}

export interface FormValidationError {
  field: string;
  message: string;
}

export type FormCheckErrors = (inputs: FormInputs) => FormValidationError[];
export type FormValidateInputs = (inputs: FormInputs) => boolean;
export type FormNormalizeInputs = (inputs: FormInputs) => void;
