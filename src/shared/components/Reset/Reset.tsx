import React from 'react';
import { useDispatch } from 'react-redux';

import * as TPS from './Reset.types';

import Button from '../Button';

import { resetProcess } from '../../entities/processes/processes.state';

import './Reset.styles.scss';

const Reset: React.FC<TPS.ResetProps> = (props) => {
  const { label } = props;

  const dispatch = useDispatch();

  return (
    <div className="b-reset">
      <Button
        label={label}
        size="big"
        type="stroked"
        bgColor="gray"
        isFetching={false}
        handlerSubmit={() => {
          dispatch(resetProcess(null))
        }}
      />
    </div>
  );
}

Reset.defaultProps = {
  label: 'отмена'
};

export default Reset;
