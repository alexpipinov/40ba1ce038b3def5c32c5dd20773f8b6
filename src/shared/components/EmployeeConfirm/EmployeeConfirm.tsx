import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import * as TPS from './EmployeeConfirm.types';
import * as F_TPS from "../Form/Form.types";
import * as EMP_CNT from "../../entities/employees/employees.constants";
import * as EMP_TPS from "../../entities/employees/employees.types";

import Form from "../Form";
import Reset from '../Reset';

import { isValidName, isValidPhone } from "../../lib/validation/validation";
import cleanPhoneInput from "../../lib/cleanPhoneInput/cleanPhoneInput";

import { RootState } from '../../store/store.types';

import { employeeSync } from "../../entities/employees/employees";
import { employeeUpdateStatus } from '../../entities/employees/employees.state';

const EmployeeConfirm: React.FC<TPS.EmployeeConfirmProps> = (props) => {
  const dispatch = useDispatch();

  const { masterId, stepName, completeStep } = props;

  const employees = useSelector((state: RootState) => state.employees);
  const employee = employees[masterId];

  const sync = useCallback(async (data: EMP_TPS.EmployeeSyncData) => {
    const response = await employeeSync(data, masterId);

    if (response.success) {
      dispatch(employeeUpdateStatus({
        uid: masterId,
        status: EMP_CNT.EMPLOYEES_STATUS_REGISTERED
      }));
      completeStep(stepName);
    }

    return response;
  }, [employees]);

  const generateFieldsForConfirmForm = useCallback(() => {
    const confirmFields: F_TPS.FormFields[] = [
      {
        type: 'text',
        name: 'name',
        placeholder: 'Имя Фамилия',
        defaultValue: employee?.data?.name,
        customValidator: {
          message: 'Введите имя и фамилию через пробел',
          method: isValidName
        },
        required: true,
        readOnly: true
      },
      {
        type: 'mask',
        name: 'phone',
        mask: '+7 (999) 999-99-99',
        placeholder: 'Телефон сотрудника',
        defaultValue: employee?.data?.phone,
        customValidator: {
          message: 'Некорректный телефон',
          method: isValidPhone
        },
        normalizeInput: cleanPhoneInput,
        required: true,
        readOnly: true
      },
      {
        type: 'mask',
        name: 'code',
        mask: '9999',
        placeholder: 'Введите код',
        required: true
      }
    ];

    return confirmFields;
  }, [employees]);

  return (
    <div>
      <Form
        key="employeeSyncForm"
        descr="Для подтверждения регистрации введите код из SMS"
        fields={generateFieldsForConfirmForm()}
        submitLabel="Подтвердить"
        onSubmit={sync}
      />

      <Reset />
    </div>
  );
}

export default EmployeeConfirm;
