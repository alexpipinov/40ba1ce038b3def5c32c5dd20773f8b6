import * as OP_TPS from '../../../content-scripts/screens/Order/components/OrderProcessing/OrderProcessing.types';

export interface EmployeeConfirmProps {
  masterId: number;
  stepName: OP_TPS.OrderProcessingSteps;
  completeStep: (step: string) => void;
}
