import React from 'react';

export interface ButtonProps {
  id?: string;
  type?: 'filled' | 'stroked';
  size?: string;
  label?: string;
  bgColor: 'gray' | 'green' | 'red' | 'blue';
  preloaderColor: string;
  isFetching: boolean;
  icon?: React.ReactNode;
  disabled?: boolean;
  handlerSubmit?(e: React.MouseEvent): void;
}
