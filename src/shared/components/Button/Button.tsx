import React from 'react';
import classNames from 'classnames';

import { ButtonProps } from './Button.types';
import './Button.styles.scss';

import Preloader from '../Preloader';

const Button = (props: ButtonProps) => {
  const {
    id,
    type,
    size,
    label,
    bgColor,
    disabled,
    preloaderColor,
    icon,
    isFetching,
    handlerSubmit
  } = props;

  const styles = classNames('b-button', `_${size}`, `_${type}`, `_bg-color-${bgColor}`, {
    '_is-active': isFetching
  })

  return (
    <button
      id={id}
      className={styles}
      disabled={disabled || isFetching}
      onClick={handlerSubmit}
    >
      {!isFetching
        ?
        <div className="b-button__label-wrap">
          <span className="b-button__label">{label}</span>
          {!icon ? null : <span className="b-button__icon">{icon}</span>}
        </div>
        :
        <div className="b-button__preloader-wrapper">
          <Preloader
            color={preloaderColor}
          />
        </div>}
    </button>
  );
}

Button.defaultProps = {
  id: null,
  type: 'filled',
  size: 'small',
  bgColor: 'green',
  disabled: false,
  preloaderColor: 'white',
  label: 'Подтвердить'
};

export default Button;
