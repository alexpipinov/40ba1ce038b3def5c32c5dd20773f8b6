export interface InlineNotificationProps {
  type?: 'errors' | 'warnings' | 'success';
  title: string;
  descr?: string;
  items?: InlineNotification[];
}

export interface InlineNotification {
  message: string;
}
