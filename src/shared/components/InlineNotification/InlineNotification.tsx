import React from 'react';
import classNames from 'classnames';

import * as TPS from './InlineNotifiation.types';

import './InlineNotification.styles.scss';

const InlineNotification: React.FC<TPS.InlineNotificationProps> = (props) => {
  const { type, title, descr, items } = props;

  const wrapperStyles = classNames('b-inline-notification__wrapper', {
    _errors: type === 'errors',
    _warnings: type === 'warnings',
    _success: type === 'success'
  });

  return (
    <div className="b-inline-notification">
      <div className={wrapperStyles}>
        <div className="b-inline-notification__title">
          { title }
        </div>

        {!descr ? null :
          <div className="b-inline-notification__descr">
            { descr }
          </div>}

        {!items || !items.length ? null :
          <ul className="b-inline-notification__items">
            {
              items.map((item, i) => (
                <li
                  key={`${type}_${i}`}
                  className="b-inline-notification__item"
                >
                  { item.message }
                </li>
              ))
            }
          </ul>}
      </div>
    </div>
  );
};

InlineNotification.defaultProps = {
  type: 'warnings',
  descr: 'Внимание!',
  items: []
};

export default InlineNotification;
