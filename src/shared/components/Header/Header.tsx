import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { RootState } from '../../store/store.types';

import * as TPS from './Header.types';
import * as P_TPS from '../../entities/processes/processes.constants';

import CloseIcon from '../Icons/CloseIcon';

import './Header.styles.scss';

const Header: React.FC<TPS.HeaderProps> = (props) => {
  const { processes } = useSelector((state: RootState) => state);

  const closePortal = useCallback((e) => {
    e.preventDefault();
    e.stopPropagation();
    props.handleClose();
  }, []);

  function getTitle() {
    const activeProcessId = processes.activeProcessId as number;
    const process = processes.list[activeProcessId];

    if (!process) {
      return 'АбсолютКарта';
    }

    switch (process.status) {
      case P_TPS.PROCESSES_STATUS_EMPLOYEE_REGISTER:
        return 'Регистрация сотрудника';
      case P_TPS.PROCESSES_STATUS_EMPLOYEE_CONFIRM:
        return 'Ожидаем подтверждения';
      case P_TPS.PROCESSES_STATUS_CLIENT_IDENTIFY:
        return 'Идентификация клиента';
      case P_TPS.PROCESSES_STATUS_CLIENT_CONFIRM_ACTIVATION:
        return 'Подтверждение регистрации';
      case P_TPS.PROCESSES_STATUS_CLIENT_PROFILE:
        return 'Выберите тип скидки';
      case P_TPS.PROCESSES_STATUS_CONFIRM_OPERATION:
        return 'Требуется подтверждение';
      case P_TPS.PROCESSES_STATUS_ERROR:
        return 'Произошла ошибка';
      default:
        return 'АбсолютКарта'
    }
  }

  return (
    <header className="b-header">
      <div className="b-header__title">
        {getTitle()}
      </div>

      <a
        href="#"
        className="b-header__close-btn"
        onClick={closePortal}
      >
        <span className="b-header__close-icon">
          <CloseIcon />
        </span>
      </a>
    </header>
  );
}

export default Header;
