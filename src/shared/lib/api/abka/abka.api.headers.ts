import * as TPS from './abka.api.types';

export const getHeaders: TPS.GetHeaders = () => ({
  'Content-Type': 'application/json'
});
