import * as TPS from "./abka.api.types";
import * as LIB from "./abka.api.lib";

import { getHeaders } from "./abka.api.headers";
import { getBody } from "./abka.api.body";

export const callAbKa: TPS.CallApi = async (endpoint, options = {}) => {
  const fullUrl = `${ABKA_API_HOST}/${ABKA_API_ENDPOINT_PREFIX}/${endpoint}`;
  const { uid, data = {} } = options;

  const body = await getBody(data, uid);

  const fetchOptions: RequestInit = {
    method: "POST",
    mode: "cors",
    headers: getHeaders(),
    body: JSON.stringify(body),
  };

  return fetch(fullUrl, fetchOptions)
    .then((response) => response.json())
    .then((response) => ({
      status: response.status,
      success: LIB.normalizeStatus(response.status),
      messages: LIB.normalizeMessages(response.messages),
      data: response.data,
    }))
    .catch((error) => {
      console.log(`[CATCH: abka.api.call - ${endpoint}]`, error);

      return {
        status: 500,
        success: false,
        messages: [],
        data: null,
      };
    });
};
