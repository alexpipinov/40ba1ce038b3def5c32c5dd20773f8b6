export type CallApi = (
  endpoint: string,
  options?: CallApiOptions,
) => Promise<ApiRequestNormalizedResponse>;

export interface CallApiOptions {
  uid?: number;
  data?: RequestInnerData;
}

export type GetHeaders = () => HeadersInit;

export interface RequestInnerData {
  [key: string]: any;
}

export interface BodySharedFields {
  sellerPlaceExtId: string;
  employeeExtId: string;
}

export interface Body extends BodySharedFields {
  [key: string]: any;
}

export type GetBody = (data: RequestInnerData, uid?: number) => Promise<Body>;

export interface AbKaResponseMessage {
  field: string;
  type: string;
  message: string;
}

export type NormalizeStatus = (status: number) => boolean;
export type NormalizeMessages = (messages: AbKaResponseMessage[]) => ErrorMessage[];
