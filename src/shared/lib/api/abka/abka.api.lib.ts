import * as TPS from './abka.api.types';
import * as CNT from './abka.api.constants';

export const normalizeStatus: TPS.NormalizeStatus = (status: number) => {
  return (
    status === CNT.RESPONSE_STATUS_SUCCESS
    || status === CNT.RESPONSE_STATUS_CREATED
  );
}

export const normalizeMessages: TPS.NormalizeMessages = (messages) => {
  if (!messages) {
    return [];
  }

  return (
    messages
      .filter(msg => msg.type === 'error')
      .map(msg => ({
        field: msg.field,
        message: msg.message
      }))
  );
}
