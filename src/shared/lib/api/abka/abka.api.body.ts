import * as TPS from './abka.api.types';

import * as YC_CNT from '../../../entities/yclients/yclients.constants';

export const getBody: TPS.GetBody = async (data, uid) => {
  const state = await chrome.storage.local.get([YC_CNT.YCLIENTS_STORAGE_KEY]);
  const session = state[YC_CNT.YCLIENTS_STORAGE_KEY];

  const sellerPlaceExtId = session.salonId.toString();
  const employeeExtId = `${uid ? uid : session.uid}`;

  return {
    sellerPlaceExtId,
    employeeExtId,
    ...data
  };
}
