import * as TPS from './yclients.api.types';
import * as YC_CNT from '../../../entities/yclients/yclients.constants';

const getToken: TPS.GetToken = async () => {
  const store = await chrome.storage.local.get([YC_CNT.YCLIENTS_STORAGE_KEY]);
  const session = store[YC_CNT.YCLIENTS_STORAGE_KEY];

  return session.token;
}

export const getHeaders: TPS.GetHeaders = async () => {
  const token = await getToken();

  return {
    Accept: 'application/vnd.yclients.v2+json',
    Authorization : token,
    'Content-Type': 'application/json'
  }
};
