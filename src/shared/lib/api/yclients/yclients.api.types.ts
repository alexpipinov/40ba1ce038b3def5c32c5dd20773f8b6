export interface YClientsResponse {
  success: boolean;
  meta: YClientsMeta;
  data: any;
}

export interface YClientsMeta {
  errors?: {
    [key: string]: string[];
  },
  message?: ErrorMessage;
}

export interface YClientsNormalizedResponse {
  status: number;
  success: boolean;
  messages: ErrorMessage[];
  data: any;
}

export type CallApi = (endpoint: string, method?: string, data?: object) => Promise<YClientsNormalizedResponse>;
export type FormatYClientsErrors = (meta: YClientsMeta) => ErrorMessage[];

export type GetToken = () => Promise<string>;
export type GetHeaders = () => Promise<HeadersInit>;
