import * as TPS from './yclients.api.types';

export const formatYClientsErrors: TPS.FormatYClientsErrors = (meta: TPS.YClientsMeta)  => {
  const result: ErrorMessage[] = [];

  if (!meta || !meta.errors) {
    return [] as ErrorMessage[];
  }

  const errorsKeys = Object.keys(meta.errors);

  errorsKeys.forEach((errorKey: string) => {
    const error = meta.errors && meta.errors[errorKey];
    const message = !error ? '' : error.reduce((acc, text) => acc + `${text}`, '');

    result.push({
      field: errorKey,
      message
    });
  });

  return result;
}
