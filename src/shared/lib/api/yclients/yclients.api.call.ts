import * as TPS from './yclients.api.types';
import * as LIB from './yclients.api.lib';

import { getHeaders } from './yclients.api.headers';

export const callYClients: TPS.CallApi = async (
  endpoint,
  method= 'GET',
  data
) => {
  const fullUrl = `https://api.yclients.com/api/v1/${endpoint}`;
  const headers = await getHeaders();

  const options: RequestInit = {
    method,
    headers,
    body: data ? JSON.stringify(data) : null
  };

  return fetch(fullUrl, options)
    .then(result => result.json())
    .then(result => {
      const { success, data, meta } = result;
      const messages = LIB.formatYClientsErrors(meta);

      return {
        status: success ? 200 : 400,
        success,
        data,
        messages
      }
    });
}
