export default (phoneInput: string) => {
  const cleanedPhone = phoneInput.replace(/[^\d]/g, "");

  if (cleanedPhone.length > 10) {
    return cleanedPhone.slice(1);
  }

  return cleanedPhone;
}
