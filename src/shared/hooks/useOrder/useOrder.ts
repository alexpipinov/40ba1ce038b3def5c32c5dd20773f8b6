import { useSelector } from 'react-redux';

import * as ST_TPS from '../../store/store.types';

const useOrder = () => {
  const { processes, orders } = useSelector((state: ST_TPS.RootState) => state);
  const { activeProcessId } = processes;

  if (!activeProcessId) {
    return null;
  }

  return orders.list[activeProcessId];
}

export default useOrder;
