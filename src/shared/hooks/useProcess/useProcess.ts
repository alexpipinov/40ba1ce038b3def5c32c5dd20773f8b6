import { useSelector } from 'react-redux';

import * as ST_TPS from '../../store/store.types';

const useProcess = (id?: number | null) => {
  const { processes } = useSelector((state: ST_TPS.RootState) => state);
  const { activeProcessId } = processes;
  const targetId = id || activeProcessId as number;

  return processes.list[targetId];
}

export default useProcess;
