import * as YC_TPS from '../shared/entities/yclients/yclients.types';

export type GetSessionData = () => YC_TPS.YClientsSessionData;
