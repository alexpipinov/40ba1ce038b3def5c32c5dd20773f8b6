import * as TPS from './session.types';

export function getEmployeeExtId() {
  return ms['uid'];
}

export function getSellerPlaceExtId() {
  return ms['salon_id'];
}

export function getReloadId() {
  return ms['reload_id'];
}

export function getToken() {
  return ms['auth'].getBearerUserAuthorizationString();
}

export const getSessionData: TPS.GetSessionData = () => ({
  uid: getEmployeeExtId(),
  salonId: getSellerPlaceExtId(),
  reloadId: getReloadId(),
  token: getToken()
});

