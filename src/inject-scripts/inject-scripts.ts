import { yClientsStateReady } from './state';
import { getSessionData } from './session';

import { emitSessionSync, listenYClientsEvents } from './events/events';

async function bootstrap() {
  const ready = await yClientsStateReady();

  if (ready) {
    syncYClientsSession();
    listenYClientsEvents();
  }
}

function syncYClientsSession() {
  const sessionData = getSessionData();
  emitSessionSync(sessionData);
}


bootstrap();
