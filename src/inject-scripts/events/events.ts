import * as CNT from './events.constants';
import * as TPS from './events.types';

import * as OR_TPS from '../../shared/entities/orders/orders.types';

export const emitSessionSync: TPS.emitSessionSync = (data) => {
  window.postMessage({
    type: CNT.INJECT_EVENT_SESSION_SYNC,
    payload: data
  });
}

export const emitPaymentTabSelected = () => {
  window.postMessage({
    type: CNT.INJECT_EVENT_PAYMENT_TAB_SELECTED
  });
}

export const emitRecordUpdated = (data: OR_TPS.Order) => {
  window.postMessage({
    type: CNT.INJECT_EVENT_RECORD_UPDATED,
    payload: data
  });
}

export const emitRecordInitialized = (recordId: number) => {
  window.postMessage({
    type: CNT.INJECT_EVENT_RECORD_INITIALIZED,
    payload: {
      recordId
    }
  })
}

export const emitRecordClosed = () => {
  window.postMessage({
    type: CNT.INJECT_EVENT_RECORD_CLOSED
  });
}

function listenRecordTabSwitchEvent() {
  ms.emitter.on('legacy-change-approved:record-modal_tab', (el: HTMLElement) => {
    const target = el.getAttribute('data-target');

    if (target === 'body_payments_2') {
      emitPaymentTabSelected();
    }
  });
}

function listenAfterUpdateRecordEvent() {
  ms.emitter.on('after-update:record-modal_visit-info', (data: OR_TPS.Order) => {
    emitRecordUpdated(data);
  });
}

function listenAfterInitializeRecordEvent() {
  ms.emitter.on('after-initialize:record-modal_record-view', (record: TPS.AfterInitializeRecord) => {
    emitRecordInitialized(record.id);
  });
}

function listenAfterCloseRecordEvent() {
  ms.emitter.on("after-close:record-modal", () => {
    emitRecordClosed();
  });
}

export function listenYClientsEvents() {
  listenAfterInitializeRecordEvent();
  listenAfterUpdateRecordEvent();
  listenRecordTabSwitchEvent();
  listenAfterCloseRecordEvent();
}
