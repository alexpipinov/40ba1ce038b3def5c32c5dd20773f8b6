import * as Y_TPS from '../../shared/entities/yclients/yclients.types';

export type emitSessionSync = (data: Y_TPS.YClientsSessionData) => void;

export interface AfterInitializeRecord {
  id: number;
}
