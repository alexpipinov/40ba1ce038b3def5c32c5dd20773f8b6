const CHECK_INTERVAL = 1000;
const REQUIRED_FIELDS = ['auth', 'uid'];

function fieldsExits(keys: string[]) {
  if (!ms) {
    return false;
  }

  for (const key of keys) {
    if (!ms[key]) {
      return false;
    }
  }

  return true;
}

/**
 * Check window.ms. It should contain required fields
 * YCLIENTS system store all required for plugin data in global variable ms
 * ms object is filling while site is loading.
 */
export function yClientsStateReady() {
  return new Promise((resolve) => {
    const interval = setInterval(() => {
      if (fieldsExits(REQUIRED_FIELDS)) {
        clearInterval(interval);
        resolve(true);
      }
    }, CHECK_INTERVAL);
  });
}
