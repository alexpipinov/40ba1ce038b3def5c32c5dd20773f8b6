declare const ms: any;

declare const ABKA_API_HOST: string;
declare const ABKA_API_ENDPOINT_PREFIX: string;
declare const NODE_ENV: string;
declare const ABKA_SUPPORT_PHONE: string;
declare const ABKA_PLUGIN_VERSION: number;

interface ErrorMessage {
  field?: string;
  message: string;
}

type ApiRequestNormalizedResponse = {
  status: number;
  success: boolean;
  messages: ErrorMessage[];
  data?: any;
}
