export const injectScripts = () => {
  const path = chrome.runtime.getURL('inject-scripts.js');
  const script = document.createElement('script');

  script.setAttribute('id', 'abka');
  script.setAttribute('type', 'module');
  script.setAttribute('src', path);

  document.body.appendChild(script);
}
