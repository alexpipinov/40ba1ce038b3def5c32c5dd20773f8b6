import React, { useState, useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '../../../shared/store/store.types';

import * as EXP_TPS from '../../../shared/entities/export/export.types';

import * as EXP_CNT from '../../../shared/entities/export/export.constants';
import * as EMP_CNT from '../../../shared/entities/employees/employees.constants';

import EmployeeRegister from '../../../shared/components/EmployeeRegister';
import EmployeeConfirm from '../../../shared/components/EmployeeConfirm';
import StatusInfo from '../../../shared/components/StatusInfo';

import Button from '../../../shared/components/Button';
import Header from '../../../shared/components/Header';
import Portal from '../../../shared/components/Portal';
import Preloading from '../../../shared/components/Preloading';

import { exportGetYClientsServices, exportToAbka } from '../../../shared/entities/export/export';
import { employeeCheckIn, employeeGetYClientsAdminInfo } from '../../../shared/entities/employees/employees';
import { setStatus } from '../../../shared/entities/export/export.state';

import './Export.styles.scss';

const Export: React.FC<{toggleVisibility: Function}> = (props) => {
  const dispatch = useDispatch();
  const [masterId, setMasterId] = useState<number | null>(null);
  const [isFetching, setFetching] = useState<boolean>(false);

  const employees = useSelector((state: RootState) => state.employees);
  const exportState = useSelector((state: RootState) => state.export);

  useEffect(() => {
    employeeCheckIn().then(check => {
      switch (check.status) {
        case EMP_CNT.EMPLOYEES_STATUS_REGISTERED:
          dispatch(setStatus(EXP_CNT.EXPORT_STATUS_ACTIVE));
          break;
        case EMP_CNT.EMPLOYEES_STATUS_NOT_REGISTERED:
          dispatch(setStatus(EXP_CNT.EXPORT_STATUS_EMPLOYEE_REGISTER));
          break;
        case EMP_CNT.EMPLOYEES_STATUS_BLOCKED:
          dispatch(setStatus(EXP_CNT.EXPORT_STATUS_EMPLOYEE_BLOCKED));
          break;
        default:
          dispatch(setStatus(EXP_CNT.EXPORT_STATUS_ERROR));
          break;
      }
    });
  }, []);

  useEffect(() => {
    employeeGetYClientsAdminInfo().then(result => {
      setMasterId(result && result.id);
    });
  }, []);

  const startExport = useCallback(async () => {
    setFetching(true);

    const services = await exportGetYClientsServices();
    const exportResult = await exportToAbka(services);

    if (exportResult.success) {
      dispatch(setStatus(EXP_CNT.EXPORT_STATUS_COMPLETE));
    } else {
      dispatch(setStatus(EXP_CNT.EXPORT_STATUS_ERROR));
    }

    setFetching(false);
  }, []);

  function stepCompleteWatcher (step: EXP_TPS.ExportStatus, success?: boolean) {
    switch (step) {
      case EXP_CNT.EXPORT_STATUS_EMPLOYEE_REGISTER:
        dispatch(setStatus(EXP_CNT.EXPORT_STATUS_EMPLOYEE_CONFIRM));
        break;
      case EXP_CNT.EXPORT_STATUS_EMPLOYEE_CONFIRM:
        dispatch(setStatus(EXP_CNT.EXPORT_STATUS_ACTIVE));
        break;
      default:
        break;
    }
  }

  const renderByStatus = () => {
    switch (exportState.status) {
      case EXP_CNT.EXPORT_STATUS_EMPLOYEE_REGISTER: {
        if (!masterId) {
          return null;
        }

        return (
          <EmployeeRegister
            context="admin"
            stepName={EXP_CNT.EXPORT_STATUS_EMPLOYEE_REGISTER}
            masterId={masterId}
            completeStep={stepCompleteWatcher}
          />
        );
      }

      case EXP_CNT.EXPORT_STATUS_EMPLOYEE_CONFIRM: {
        if (!masterId) {
          return;
        }

        return (
          <EmployeeConfirm
            stepName={EXP_CNT.EXPORT_STATUS_EMPLOYEE_CONFIRM}
            masterId={masterId}
            completeStep={stepCompleteWatcher}
          />
        );
      }
      case EXP_CNT.EXPORT_STATUS_EMPLOYEE_BLOCKED:
        return (
          <StatusInfo
            text="Сотрудник заблокирован в системе «АбсолютКарта». Обратитесь в поддержку программы лояльности"
            type="error"
          />
        );
      case EXP_CNT.EXPORT_STATUS_ACTIVE:
        return (
          <React.Fragment>
            <div className="b-export__descr">
              Экспортируйте услуги в программу лояльности «АбсолютКарта», чтобы их можно было использовать в качесте подарков
            </div>
            <Button
              label="Экспортировать услуги"
              bgColor="green"
              size="big"
              isFetching={isFetching}
              handlerSubmit={startExport}
            />
          </React.Fragment>
        );
      case EXP_CNT.EXPORT_STATUS_COMPLETE:
        return (
          <StatusInfo
            title="Все прошло успешно!"
            text="Можете закрыть окно"
            type="success"
          />
        );
      case EXP_CNT.EXPORT_STATUS_ERROR:
        return (
          <StatusInfo
            text="Произошла ошибка! Перезагрузите страницу и попробуйте снова"
            type="error"
          />
        );
      default:
        return null;
    }
  }

  return (
    <Portal>
      <React.Fragment>
        <Header handleClose={props.toggleVisibility}/>

        <div className="b-export">
          <div className="b-export__content-wrap">
            {renderByStatus()}
          </div>

          <div className="b-export__bg-preloader-wrap">
            <Preloading size={35} />
          </div>
        </div>
      </React.Fragment>
    </Portal>
  );
}

export default Export;
