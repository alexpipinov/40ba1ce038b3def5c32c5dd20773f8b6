import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';


import Export from './Export';

import store from '../../../shared/store/configureStore';

class Root {
  node: HTMLElement;

  constructor() {
    this.initializeNode();

    this.render();
  }

  initializeNode() {
    this.node = document.createElement('div');
    this.node.setAttribute('id', 'abka-export');

    document.body.append(this.node);
  }

  render() {
    ReactDOM.render(
      <Provider store={store}>
        <Export toggleVisibility={this.destroy.bind(this)} />
      </Provider>,
      this.node
    );
  }

  destroy() {
    document.body.removeChild(this.node);
    ReactDOM.unmountComponentAtNode(this.node);

    window.postMessage({type: 'P_CLOSE'});
  }
}

export default Root;
