import * as IN_CNT from '../../inject-scripts/events/events.constants';

import store from '../../shared/store/configureStore';
import { initProcess } from '../../shared/entities/processes/processes.state';
import { reset } from '../../shared/entities/export/export.state';

import Order from './Order/Order';
import Export from './Export';

let activePlugin: any = null;
let activeExport: any = null;

const listenOrderPluginEvents = () => {
  window.addEventListener('message', (e) => {
    if (typeof e.data === 'string') {
      return;
    }

    const event = e.data?.type;

    if (event === IN_CNT.INJECT_EVENT_RECORD_INITIALIZED) {
      const { place } = store.getState();
      const { recordId } = e.data.payload;
      const allowRenderPluginBtn = place.status !== 'not-registered';

      if (!activePlugin && !activeExport && e.data.payload.recordId && allowRenderPluginBtn) {
        activePlugin = new Order(recordId);

        store.dispatch(initProcess(recordId));
      }
    }

    if (event === IN_CNT.INJECT_EVENT_RECORD_CLOSED) {
      activePlugin && activePlugin.destroy();
      activePlugin = null;
    }
  });
}

const listenExportPluginEvents = () => {
  chrome.runtime.onMessage.addListener((e) => {
    const event = e.data?.type;

    if (event === 'P_EXPORT' && !activeExport && !activePlugin) {
      activeExport = new Export();
    }
  });

  window.addEventListener('message', (e) => {
    if (typeof e.data === 'string') {
      return;
    }

    if (e.data.type === 'P_CLOSE') {
      activeExport = null;
      store.dispatch(reset(null));
    }
  });
}

export const screensWorker = () => {
  listenOrderPluginEvents();
  listenExportPluginEvents();
}
