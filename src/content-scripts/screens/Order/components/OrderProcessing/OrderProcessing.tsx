import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import * as CNT from './OrderProcessing.constants';
import * as TPS from './OrderProcessing.types';

import * as P_CNT from '../../../../../shared/entities/processes/processes.constants';
import * as EMP_CNT from '../../../../../shared/entities/employees/employees.constants';
import * as ST_TPS from '../../../../../shared/store/store.types';

import Preloading from '../../../../../shared/components/Preloading';
import EmployeeRegister from '../../../../../shared/components/EmployeeRegister';
import EmployeeConfirm from '../../../../../shared/components/EmployeeConfirm';
import ClientIdentify from '../../../../../shared/components/ClientIdentify';
import ClientConfirmActivation from '../../../../../shared/components/ClientConfirmActivation';
import ClientProfile from '../../../../../shared/components/ClientProfile';
import ConfirmOperation from '../../../../../shared/components/ConfirmOperation';
import StatusInfo from '../../../../../shared/components/StatusInfo';

import cleanPhoneInput from '../../../../../shared/lib/cleanPhoneInput';
import { employeeCheckIn } from '../../../../../shared/entities/employees/employees';
import { setStatus } from '../../../../../shared/entities/processes/processes.state';

import { useProcess, useOrder } from '../../../../../shared/hooks';

import './OrderProcessing.styles.scss';

const OrderProcessing: React.FC = () => {
  const dispatch = useDispatch();

  const { processes } = useSelector((state: ST_TPS.RootState)  => state)
  const { activeProcessId } = processes;

  const process = useProcess(activeProcessId);
  const order = useOrder();

  useEffect(() => {
    if (!process || !order) {
      return;
    }

    if (process.status === P_CNT.PROCESSES_STATUS_INITIALIZING) {
      if (order.comment.includes('ABKA')) {
        dispatch(setStatus(P_CNT.PROCESSES_STATUS_COMPLETE));
      } else {
        employeeCheckIn(order.staff_id).then(check => {
          switch (check.status) {
            case EMP_CNT.EMPLOYEES_STATUS_REGISTERED:
              dispatch(setStatus(P_CNT.PROCESSES_STATUS_CLIENT_IDENTIFY));
              break;
            case EMP_CNT.EMPLOYEES_STATUS_NOT_REGISTERED:
              dispatch(setStatus(P_CNT.PROCESSES_STATUS_EMPLOYEE_REGISTER));
              break;
            case EMP_CNT.EMPLOYEES_STATUS_BLOCKED:
              dispatch(setStatus(P_CNT.PROCESSES_STATUS_EMPLOYEE_BLOCKED));
              break;
            default:
              dispatch(setStatus(P_CNT.PROCESSES_STATUS_ERROR));
              break;
          }
        });
      }
    }
  }, [process]);

  function stepCompleteWatcher (step: TPS.OrderProcessingSteps, success?: boolean) {
    switch (step) {
      case P_CNT.PROCESSES_STATUS_EMPLOYEE_REGISTER:
        dispatch(setStatus(P_CNT.PROCESSES_STATUS_EMPLOYEE_CONFIRM));
        break;
      case P_CNT.PROCESSES_STATUS_EMPLOYEE_CONFIRM:
        dispatch(setStatus(P_CNT.PROCESSES_STATUS_CLIENT_IDENTIFY));
        break;
      case P_CNT.PROCESSES_STATUS_EMPLOYEE_BLOCKED:
        dispatch(setStatus(P_CNT.PROCESSES_STATUS_EMPLOYEE_BLOCKED));
        break;
      case P_CNT.PROCESSES_STATUS_CLIENT_IDENTIFY:
        if (success) {
          dispatch(setStatus(P_CNT.PROCESSES_STATUS_CLIENT_PROFILE));
        } else {
          dispatch(setStatus(P_CNT.PROCESSES_STATUS_CLIENT_CONFIRM_ACTIVATION));
        }
        break;
      case P_CNT.PROCESSES_STATUS_CLIENT_CONFIRM_ACTIVATION: {
        dispatch(setStatus(P_CNT.PROCESSES_STATUS_CLIENT_PROFILE));
        break;
      }
      case P_CNT.PROCESSES_STATUS_CLIENT_PROFILE: {
        if (success) {
          dispatch(setStatus(P_CNT.PROCESSES_STATUS_COMPLETE));
        } else {
          dispatch(setStatus(P_CNT.PROCESSES_STATUS_CONFIRM_OPERATION));
        }
        break;
      }
      case CNT.ORDER_PROCESSING_STEP_CONFIRM_OPERATION: {
        if (success) {
          dispatch(setStatus(P_CNT.PROCESSES_STATUS_COMPLETE));
        } else {
          dispatch(setStatus(P_CNT.PROCESSES_STATUS_ERROR));
        }
        break;
      }
      default:
        dispatch(setStatus(P_CNT.PROCESSES_STATUS_ERROR));
        break;
    }
  }

  function renderByStatus () {
    if (!order || !process) {
      return null;
    }

    const { staff_id: masterId, client } = order;
    const clientPhone = cleanPhoneInput(client.phone);

    switch(process.status) {
      case P_CNT.PROCESSES_STATUS_EMPLOYEE_REGISTER:
        return (
          <div className="b-order-processing__step-wrap">
            <EmployeeRegister
              context="staff"
              stepName={P_CNT.PROCESSES_STATUS_EMPLOYEE_REGISTER}
              masterId={masterId}
              completeStep={stepCompleteWatcher}
            />
          </div>
        );
      case P_CNT.PROCESSES_STATUS_EMPLOYEE_CONFIRM:
        return (
          <div className="b-order-processing__step-wrap">
            <EmployeeConfirm
              stepName={P_CNT.PROCESSES_STATUS_EMPLOYEE_CONFIRM}
              masterId={masterId}
              completeStep={stepCompleteWatcher}
            />
          </div>
        );
      case P_CNT.PROCESSES_STATUS_EMPLOYEE_BLOCKED:
        return (
          <div className="b-order-processing__step-wrap">
            <StatusInfo
              text="Сотрудник заблокирован в системе «АбсолютКарта». Обратитесь в поддержку программы лояльности"
              type="error"
            />
          </div>
        );
      case P_CNT.PROCESSES_STATUS_CLIENT_IDENTIFY:
        return (
          <div className="b-order-processing__step-wrap">
            <ClientIdentify
              phone={clientPhone}
              masterId={masterId}
              stepName={P_CNT.PROCESSES_STATUS_CLIENT_IDENTIFY}
              completeStep={stepCompleteWatcher}
            />
          </div>
        );
      case P_CNT.PROCESSES_STATUS_CLIENT_CONFIRM_ACTIVATION:
        return (
          <div className="b-order-processing__step-wrap">
            <ClientConfirmActivation
              phone={clientPhone}
              masterId={masterId}
              stepName={P_CNT.PROCESSES_STATUS_CLIENT_CONFIRM_ACTIVATION}
              completeStep={stepCompleteWatcher}
            />
          </div>
        );
      case P_CNT.PROCESSES_STATUS_CLIENT_PROFILE:
        return (
          <div className="b-order-processing__step-wrap">
            <ClientProfile
              stepName={P_CNT.PROCESSES_STATUS_CLIENT_PROFILE}
              completeStep={stepCompleteWatcher}
            />
          </div>
        );
      case P_CNT.PROCESSES_STATUS_CONFIRM_OPERATION: {
        const { discountType } = process;

        if (!discountType) {
          return null;
        }

        return (
          <div className="b-order-processing__step-wrap">
            <ConfirmOperation
              discountType={discountType}
              masterId={masterId}
              stepName={P_CNT.PROCESSES_STATUS_CONFIRM_OPERATION}
              completeStep={stepCompleteWatcher}
            />
          </div>
        );
      }
      case P_CNT.PROCESSES_STATUS_COMPLETE:
        return (
          <div className="b-order-processing__step-wrap">
            <StatusInfo
              title="Все прошло успешно!"
              text="Можете закрыть окно и продолжить оформление заказа"
              type="success"
            />
          </div>
        );
      case P_CNT.PROCESSES_STATUS_ERROR:
        return (
          <div className="b-order-processing__step-wrap">
            <StatusInfo
              text="Произошла ошибка! Перезагрузите страницу и попробуйте снова"
              type="error"
            />
          </div>
        );
      default:
        return null;
    }
  }

  return (
    <div className="b-order-processing">
      {!process || !order ? null : renderByStatus()}

      <div className="b-order-processing__bg-preloader-wrap">
        <Preloading size={35} />
      </div>
    </div>
  );
}

export default OrderProcessing;
