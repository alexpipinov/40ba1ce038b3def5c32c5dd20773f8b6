import * as CNT from './OrderProcessing.constants';

export type OrderProcessingSteps = (
  typeof CNT.ORDER_PROCESSING_STEP_EMPLOYEE_REGISTER
  | typeof CNT.ORDER_PROCESSING_STEP_EMPLOYEE_CONFIRM
  | typeof CNT.ORDER_PROCESSING_STEP_EMPLOYEE_BLOCKED
  | typeof CNT.ORDER_PROCESSING_STEP_CLIENT_IDENTIFY
  | typeof CNT.ORDER_PROCESSING_STEP_CLIENT_CONFIRM_ACTIVATION
  | typeof CNT.ORDER_PROCESSING_STEP_CONFIRM_OPERATION
  | typeof CNT.ORDER_PROCESSING_STEP_CLIENT_PROFILE
  | typeof CNT.ORDER_PROCESSING_STEP_ERROR
);

