export const ORDER_PROCESSING_STEP_EMPLOYEE_REGISTER = 'employee-register';
export const ORDER_PROCESSING_STEP_EMPLOYEE_CONFIRM = 'employee-confirm';
export const ORDER_PROCESSING_STEP_EMPLOYEE_BLOCKED = 'employee-blocked';
export const ORDER_PROCESSING_STEP_CLIENT_IDENTIFY = 'client-identify';
export const ORDER_PROCESSING_STEP_CLIENT_CONFIRM_ACTIVATION = 'client-confirm-activation';
export const ORDER_PROCESSING_STEP_CLIENT_PROFILE = 'client-profile';
export const ORDER_PROCESSING_STEP_CONFIRM_OPERATION = 'confirm-operation';
export const ORDER_PROCESSING_STEP_ERROR = 'error';
