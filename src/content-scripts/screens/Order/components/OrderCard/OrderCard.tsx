import React, { useState, useCallback } from 'react';

import Portal from '../../../../../shared/components/Portal';
import Header from '../../../../../shared/components/Header';
import OrderProcessing from '../OrderProcessing';

import './OrderCard.styles.scss';

const OrderCard: React.FC = () => {
  const [portalIsVisible, setPortalVisibility] = useState(false);

  const togglePortalVisibility = useCallback(() => {
    setPortalVisibility(!portalIsVisible);
  }, [portalIsVisible]);

  return (
    <div
      className="b-order-card"
      onClick={togglePortalVisibility}
    >
      <img
        className="b-order-card__logo"
        src={chrome.runtime.getURL('logo.png')}
        alt="Логотип АбсолютКарта"
      />

      <div className="b-order-card__title">
        АбсолютКарта
      </div>

      <div className="b-order-card__description">
        Скидки / Бонусы / Подарки
      </div>

      {!portalIsVisible ? null :
        <Portal>
          <React.Fragment>
            <Header handleClose={togglePortalVisibility}/>
            <OrderProcessing />
          </React.Fragment>
        </Portal>}
    </div>
  );
}

export default OrderCard;
