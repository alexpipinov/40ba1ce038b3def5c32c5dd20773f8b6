import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import store from '../../../shared/store/configureStore';

import * as IN_CNT from '../../../inject-scripts/events/events.constants';

import OrderCard from './components/OrderCard';

import { orderInfo } from "../../../shared/entities/orders/orders";
import { setActiveProcessId } from "../../../shared/entities/processes/processes.state";
import { setOrder } from '../../../shared/entities/orders/orders.state';

class Order {
  orderId: string;
  screenContainer = document.createElement('div');

  constructor(orderId: string) {
    this.orderId = orderId;

    this.startProcessing();
    this.fetchOrderData();
    this.listenOrderEvents();
  }

  startProcessing() {
    store.dispatch(setActiveProcessId(this.orderId));
  }

  async fetchOrderData() {
    const order = await orderInfo(this.orderId);

    if (order) {
      store.dispatch(setOrder(order));
    }
  }

  listenOrderEvents() {
    window.addEventListener('message', this.handleMessage);
  }

  handleMessage = async (e: MessageEvent) => {
    if (typeof e.data === 'string') {
      return;
    }

    if (e.data.type === IN_CNT.INJECT_EVENT_PAYMENT_TAB_SELECTED) {
      this.prepareDom();
    }
  }

  async getTargetDomNode(): Promise<Element | null> {
    return new Promise((resolve) => {
      const interval = setInterval(() => {
        const loyaltyBox = document.querySelector('.visit-payments__quick-payment__loyalty');
        const paymentDoneBox = document.querySelector('.v-payment__result');

        if (paymentDoneBox) {
          clearInterval(interval)
          resolve(null);
        }

        if (loyaltyBox) {
          const headerNode = loyaltyBox.querySelector('.v-payment__section__h2');

          if (headerNode) {
            clearInterval(interval);
            resolve(headerNode);
          }
        }
      }, 300);
    });
  }

  async prepareDom() {
    const node = await this.getTargetDomNode();

    if (node) {
      node.after(this.screenContainer);
      this.render(this.screenContainer);
    }
  }

  render(node: HTMLElement) {
    ReactDOM.render(
      <Provider store={store}>
        <OrderCard />
      </Provider>,
      node
    );
  }

  destroy() {
    ReactDOM.unmountComponentAtNode(this.screenContainer);
    window.removeEventListener('message', this.handleMessage);
  }
}

export default Order;
