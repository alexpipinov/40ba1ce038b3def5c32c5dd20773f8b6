import { injectScripts } from './inject-scripts';
import { screensWorker } from './screens/screensWorker';
import { listenInjectedScripts } from './events/inject/listeners';

import { serviceClearCash } from '../shared/lib/services/services';

const bootstrap = async () => {
  if (NODE_ENV === 'development') {
    await serviceClearCash();
  }

  injectScripts();
  screensWorker();

  listenInjectedScripts();
}

bootstrap();
