import * as IN_CNT from '../../../inject-scripts/events/events.constants';
import * as P_CNT from '../../../shared/entities/processes/processes.constants';

import store from '../../../shared/store/configureStore';

import { yClientsSyncData } from '../../../shared/entities/yclients/yclients';
import { placeCheckIn } from "../../../shared/entities/place/place";
import { orderIsPaidWithLoyalty, orderInfo } from "../../../shared/entities/orders/orders";
import { abkaDiscountsPass } from '../../../shared/entities/abkaDiscounts/abkaDiscounts';

import { setOrder }  from '../../../shared/entities/orders/orders.state';
import { setActiveProcessId, setStatus, checkAfterUpdate } from '../../../shared/entities/processes/processes.state';

export const listenInjectedScripts = () => {
  window.addEventListener('message', async (e) => {
    if (typeof e.data === 'string') {
      return;
    }

    if (e.data.type === IN_CNT.INJECT_EVENT_SESSION_SYNC) {
      yClientsSyncData(e.data.payload);
      placeCheckIn();
    }

    if (e.data.type === IN_CNT.INJECT_EVENT_RECORD_CLOSED) {
      const canPass = await orderIsPaidWithLoyalty();

      if (canPass) {
        store.dispatch(setStatus(P_CNT.PROCESSES_STATUS_CLOSED));
        await abkaDiscountsPass();
      }

      store.dispatch(setActiveProcessId(null));
    }

    if (e.data.type === IN_CNT.INJECT_EVENT_RECORD_UPDATED) {
      store.dispatch(checkAfterUpdate(null));

      const { processes } = store.getState();
      const { activeProcessId } = processes;

      if (activeProcessId) {
        const order = await orderInfo(activeProcessId);
        store.dispatch(setOrder(order));
      }
    }
  });
}
