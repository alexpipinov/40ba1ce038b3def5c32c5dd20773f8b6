import { setPluginStatus } from './status/status';

chrome.runtime.onInstalled.addListener(async (a) => {
  await setPluginStatus();
});

chrome.tabs.onActivated.addListener(async () => {
  await setPluginStatus();
});

chrome.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
  if (changeInfo.status === 'complete') {
    await setPluginStatus();
  }
});
