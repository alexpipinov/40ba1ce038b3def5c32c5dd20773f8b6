export const ALLOWED_DOMAIN = 'yclients.com';
export const DISALLOWED_URL = 'signin';

export const ICON_STATUS_ACTIVE = 'active';
export const ICON_STATUS_DISABLED = 'disabled';
export const ICON_STATUS_ERROR = 'error';
