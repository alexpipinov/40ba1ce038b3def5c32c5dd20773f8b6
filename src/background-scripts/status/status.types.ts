export type Status = 'active' | 'disabled' | 'error';
export type IconType = 'active' | 'disabled' | 'error';
