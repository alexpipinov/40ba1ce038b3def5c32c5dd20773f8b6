import * as TPS from './status.types';
import * as CNT from './status.constants';

import { placeGetStatus } from '../../shared/entities/place/place';

function isAllowedHost(url: string | undefined) {
  return url && url.includes(CNT.ALLOWED_DOMAIN);
}

function isAllowedUrl(url: string | undefined) {
  return url && !url.includes(CNT.DISALLOWED_URL);
}

export async function setPluginStatus() {
  const queryOptions = { active: true, currentWindow: true };
  const [tab] = await chrome.tabs.query(queryOptions);

  if (!tab?.url) return;

  if (!isAllowedHost(tab.url) || !isAllowedUrl(tab.url)) {
    disablePlugin();
    setIcons(CNT.ICON_STATUS_DISABLED);
  } else {
    const placeStatus = await placeGetStatus();
    const iconType = placeStatus === 'active' ? CNT.ICON_STATUS_ACTIVE : CNT.ICON_STATUS_ERROR;

    enablePlugin();
    setIcons(iconType);
  }
}

function disablePlugin() {
  return chrome.action.disable();
}

function enablePlugin() {
  return chrome.action.enable();
}

function getIcons(type: TPS.IconType) {
  return {
    16: `./icons/${type}_icon_16.png`,
    32: `./icons/${type}_icon_32.png`,
    48: `./icons/${type}_icon_48.png`,
    64: `./icons/${type}_icon_64.png`,
    128: `./icons/${type}_icon_128.png`
  }
}

export function setIcons(type: TPS.IconType) {
  return chrome.action.setIcon({
    path: getIcons(type)
  });
}
