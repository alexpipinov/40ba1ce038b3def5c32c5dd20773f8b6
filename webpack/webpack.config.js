require('dotenv').config();

const path = require('path');

const plugins = require('./settings/plugins');
const rules = require('./settings/rules');
const optimizations = require('./settings/optimizations');

const env = process.env;

const isProduction = () => env.NODE_ENV === 'production';

const webpackConfig = {
  context: path.resolve(__dirname, '..', 'src'),
  entry: {
    'content-scripts': './content-scripts/content-scripts.ts',
    'inject-scripts': './inject-scripts/inject-scripts.ts',
    'background-scripts': './background-scripts/background-scripts.ts',
    'popup': './popup/Popup.tsx'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, '..', 'build'),
    clean: true
  },
  module: {
    rules
  },
  plugins,
  optimization: optimizations,
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.scss']
  },
  mode: env.NODE_ENV,
  watch: !isProduction()
};

if (!isProduction()) {
  webpackConfig.devtool = 'inline-source-map';
}


module.exports = webpackConfig;
