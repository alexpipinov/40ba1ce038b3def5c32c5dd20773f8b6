const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');

const packageJSON = require('../../package.json');

module.exports = [
  new webpack.DefinePlugin({
    NODE_ENV: JSON.stringify(process.env.NODE_ENV),
    ABKA_API_HOST: JSON.stringify(process.env.ABKA_API_HOST),
    ABKA_API_ENDPOINT_PREFIX: JSON.stringify(process.env.ABKA_API_ENDPOINT_PREFIX),
    ABKA_SUPPORT_PHONE: JSON.stringify(process.env.ABKA_SUPPORT_PHONE),
    ABKA_PLUGIN_VERSION: JSON.stringify(packageJSON.version),
  }),
  new CopyWebpackPlugin({
    patterns: [
      {
        from: path.resolve(__dirname, '../..', 'src/manifest.json'),
        to: path.resolve(__dirname, '../..', 'build/manifest.json')
      },
      {
        from: path.resolve(__dirname, '../..', 'src/assets/'),
        to: path.resolve(__dirname, '../..', 'build/')
      }
    ]
  }),
  new HtmlWebpackPlugin({
    title: 'АбсолютКарта',
    filename: 'popup.html',
    chunks: ['popup']
  })
];
