const TerserPlugin = require('terser-webpack-plugin');

const minimizer = [];

if (process.env.NODE_ENV === 'production') {
  minimizer.push(
    new TerserPlugin({
      minify: TerserPlugin.terserMinify,
      terserOptions: {
        compress: {
          drop_console: true,
        },
        format: {
          comments: false
        },
        mangle: true
      },
    }),
  );
}

module.exports = {
  minimize: process.env.NODE_ENV === 'production',
  minimizer
}
