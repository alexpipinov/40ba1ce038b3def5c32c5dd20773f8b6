const exclude = /node_modules/;

module.exports = [
  {
    test: /\.(js|jsx|ts|tsx)$/,
    use: 'ts-loader',
    exclude
  },
  {
    test: /\.(css|scss)$/i,
    use: [
      // Creates `style` nodes from JS strings
      "style-loader",
      // Translates CSS into CommonJS
      "css-loader",
      // Compiles Sass to CSS
      "sass-loader",
    ]
  },
  {
    test: /\.(png|jpg|jpeg|svg|gif)$/,
    exclude,
    use: ['file-loader']
  }
];
